@extends('admin.layouts.app')

@section('panel')
<div class="row mb-none-30">
    <div class="col-xl-12 col-lg-12 col-md-12 mb-30">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title border-bottom pb-2">@lang('Information of Route')</h5>

                <form action="{{ route('admin.trip.times', $trip->id)}}" method="POST">
                    @csrf

                    @foreach($stoppages as $key => $stoppage)
                        <div class="row my-2">
                            <div class="col-12 col-md-4">
                                <h5>{{$stoppage->counter->name}}</h5>
                                <p>{{$stoppage->counter->description}}</p>
                            </div>
                            <div class="col-12 col-md-8">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label class="form-control-label font-weight-bold"> @lang('Arrival')</label>
                                            <input type="text" class="form-control" name="times[{{$stoppage->id}}][]" placeholder="@lang('Enter Approximate Arrival Time')" value="{{ date ('H:i',strtotime($stoppage->arrival)) }}">
                                            @if($key==0)<small class="text-danger">@lang('Keep space between value & unit')</small>@endif

                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label class="form-control-label font-weight-bold"> @lang('Departure')</label>
                                            <input type="text" class="form-control" name="times[{{$stoppage->id}}][]" placeholder="@lang('Enter Approximate Departure Time')" value="{{ date ('H:i',strtotime($stoppage->departure)) }}">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label class="form-control-label font-weight-bold"> @lang('Crossover')</label><br>
                                            <input type="hidden" name="is_active_crossover[{{$stoppage->id}}][]" value="0">
                                            <input type="checkbox" id="is_active_crossover" value="1" name="is_active_crossover[{{$stoppage->id}}][]" @if($stoppage->crossover) checked @endif>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="row mt-4">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn--primary btn-block btn-lg">@lang('Save Changes')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('breadcrumb-plugins')
    <a href="{{ route('admin.trip.list') }}" class="btn btn-sm btn--primary box--shadow1 text--small addBtn"><i class="la la-fw la-backward"></i>@lang('Go Back')</a>
@endpush
