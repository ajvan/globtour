@extends('admin.layouts.app')

{{-- dropzone css --}}
@push('style')
<link href="{{ asset('assets/admin/css/dropzone.min.css') }}" rel="stylesheet" type="text/css">
@endpush

@section('panel')

<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.manage.accommodation.edit', $accommodation->id)}}" method="POST" enctype="multipart/form-data">

                        @csrf

                        <input type="hidden" name="id" value="{{$accommodation->id}}">
                        <div class="form-row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Image</label>
                                    <div class="image-upload">
                                        <div class="thumb">
                                            <div class="avatar-preview">
                                                <div class="profilePicPreview" style="background-image: url({{$accommodation->image ?? asset('assets/images/default.png')}})">
                                                    <button type="button" class="remove-image"><i class="fa fa-times"></i></button>
                                                </div>
                                            </div>
                                            <div class="avatar-edit">
                                                <input type="file" class="profilePicUpload" name="image" id="profilePicUpload0" accept=".png, .jpg, .jpeg">
                                                <label for="profilePicUpload0" class="bg--primary">Image</label>
                                                <small class="mt-2 text-facebook">Supported files: <b>jpeg, jpg, png</b>. Will be resized to: <b>600x400</b>px.</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>@lang('Title')</label>
                                        <input type="text" class="form-control" placeholder="Title" name="name" value="{{$accommodation->name}}" required/>
                                    </div>
                                </div>

                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label>@lang('Language')</label>
                                        <select name="lang" placeholder="Language" class="form-control">
                                            @if(!$accommodation->lang)<option value="">@lang('Select a language')</option>@endif
                                            <option value="sr" @if($accommodation->lang =='sr') selected @endif>Srpski</option>
                                            <option value="en" @if($accommodation->lang =='en') selected @endif>English</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>@lang('Description')</label>
                                        <textarea rows="10" class="form-control nicEdit" placeholder="Description" name="description">{!! $accommodation->description !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn--primary btn-block btn-lg">Update</button>
                        </div>
                </form>

                <hr class="my-5">

                <h4><a href="{{route('admin.manage.accommodation.show', $accommodation->id)}}">Refresh</a></h4>

                <div class="row">
                    @foreach($accommodation->media as $image)
                    <div class="col-md-3">

                        {{-- <div class="thumb">
                            <div class="avatar-preview">
                                <div class="profilePicPreview" style="background-image: url({{$image->url}})">
                                    <button type="button" class="remove-image"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                        </div> --}}

                        <img src="{{$image->url}}" class="img-thumbnail">
                        <span>
                            <a class="btn btn-sm btn-danger" href="{{route('admin.manage.accommodation.media.remove', $image->id)}}"><i class="fas fa-trash-alt"></i></a>
                        </span>
                    </div>


                    @endforeach
                </div>

                <hr class="my-5">

                <div class="row">
                    <div class="col-12">
                        <div class="form-group my-5 mr-2 ml-2">
                            <label>@lang('Upload Image')</label>
                            <form action="{{route('admin.manage.accommodation.media.upload', $accommodation->id)}}" class="dropzone bg-light" id="my-awesome-dropzone" enctype="multipart/form-data">{{ csrf_field() }}</form>
                            <p class="help-block">max file size: 5mb</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection



@push('breadcrumb-plugins')
    <a href="{{route('admin.manage.accommodation')}}" class="btn btn-sm btn--primary box--shadow1 text--small"><i class="fa fa-fw fa-backward"></i>@lang('Go Back')</a>
@endpush

@push('script-lib')
    <script src="{{ asset('assets/admin/js/bootstrap-iconpicker.bundle.min.js') }}"></script>
@endpush

@push('script')
    <script>
        (function ($) {
            "use strict";
            $('.iconPicker').iconpicker().on('change', function (e) {
                $(this).parent().siblings('.icon').val(`<i class="${e.icon}"></i>`);
            });
        })(jQuery);
    </script>


<!-- UPLOAD IMAGE-->
<script src="{{ asset('assets/admin/js/dropzone.min.js') }}"></script>
@endpush
