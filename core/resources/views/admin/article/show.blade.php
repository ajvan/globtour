@extends('admin.layouts.app')

{{-- dropzone css --}}
{{-- @push('style')
<link href="{{ asset('assets/admin/css/dropzone.min.css') }}" rel="stylesheet" type="text/css">
@endpush --}}

@section('panel')

<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.frontend.blog.article.edit', $article->id)}}" method="POST" enctype="multipart/form-data">

                        @csrf

                        <input type="hidden" name="id" value="{{$article->id}}">
                        <div class="form-row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Image</label>
                                    <div class="image-upload">
                                        <div class="thumb">
                                            <div class="avatar-preview">
                                                <div class="profilePicPreview" style="background-image: url({{$article->image ?? asset('assets/images/default.png')}})">
                                                    <button type="button" class="remove-image"><i class="fa fa-times"></i></button>
                                                </div>
                                            </div>
                                            <div class="avatar-edit">
                                                <input type="file" class="profilePicUpload" name="image" id="profilePicUpload0" accept=".png, .jpg, .jpeg">
                                                <label for="profilePicUpload0" class="bg--primary">Image</label>
                                                <small class="mt-2 text-facebook">Supported files: <b>jpeg, jpg, png</b>. Will be resized to: <b>600x400</b>px.</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>@lang('Title')</label>
                                        <input type="text" class="form-control" placeholder="Title" name="title" value="{{$article->title}}" required/>
                                    </div>
                                </div>

                                <div class="col-12 col-md-4">
                                    <div class="form-group">
                                        <label>@lang('Language')</label>
                                        <select name="lang" placeholder="Language" class="form-control">
                                            @if(!$article->lang)<option value="">@lang('Select a language')</option>@endif
                                            <option value="sr" @if($article->lang =='sr') selected @endif>Srpski</option>
                                            <option value="en" @if($article->lang =='en') selected @endif>English</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>@lang('Description')</label>
                                        <textarea rows="10" class="form-control nicEdit" placeholder="Description" name="content">{!! $article->content !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn--primary btn-block btn-lg">Update</button>
                        </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection



@push('breadcrumb-plugins')
    <a href="{{route('admin.frontend.blog.article')}}" class="btn btn-sm btn--primary box--shadow1 text--small"><i class="fa fa-fw fa-backward"></i>@lang('Go Back')</a>
@endpush

@push('script-lib')
    <script src="{{ asset('assets/admin/js/bootstrap-iconpicker.bundle.min.js') }}"></script>
@endpush

@push('script')
    <script>
        (function ($) {
            "use strict";
            $('.iconPicker').iconpicker().on('change', function (e) {
                $(this).parent().siblings('.icon').val(`<i class="${e.icon}"></i>`);
            });
        })(jQuery);
    </script>


<!-- UPLOAD IMAGE-->
{{-- <script src="{{ asset('assets/admin/js/dropzone.min.js') }}"></script> --}}
@endpush
