@extends('admin.layouts.app')

@section('panel')
    <div class="row">
        <div class="col-md-12">
            <div class="card b-radius--10 ">
                <div class="card-body">
                    <div class="table-responsive--sm table-responsive">
                        <table class="table table--light style--two">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>@lang('Title')</th>
                                    <th>@lang('Slug')</th>
                                    <th>@lang('Description')</th>
                                    <th>@lang('Status')</th>
                                    <th>@lang('Action')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse($articles as $item)
                                <tr>
                                    <td data-label="ID">
                                        {{ $item->id }}
                                    </td>
                                    <td data-label="@lang('Name')">
                                        {{ __($item->title) }}
                                    </td>
                                    <td data-label="@lang('Slug')">
                                        {{ __($item->slug) }}
                                    </td>
                                    <td data-label="@lang('Description')">
                                        {{ __($item->description) }}
                                    </td>
                                    <td data-label="@lang('Status')">
                                        @if($item->status == 1)
                                        <span class="text--small badge font-weight-normal badge--success">@lang('Active')</span>
                                        @else
                                        <span class="text--small badge font-weight-normal badge--warning">@lang('Disabled')</span>
                                        @endif
                                    </td>
                                    <td data-label="@lang('Action')">

                                        <a href="{{route('admin.frontend.blog.article.show', $item->id)}}" class="icon-btn"><i class="la la-eye"></i></a>

                                        <button type="button" class="icon-btn ml-1 editBtn"
                                                data-toggle="modal" data-target="#editModal"
                                                data-article="{{ $item }}"
                                                data-action="{{ route('admin.frontend.blog.article.update', $item->id) }}"
                                                data-original-title="@lang('Update')">
                                            <i class="la la-pen"></i>
                                        </button>

                                        <button type="submit" form="remove-{{$item->id}}"
                                            class="icon-btn btn--danger ml-1"
                                            data-original-title="@lang('Delete')">
                                            <i class="la la-trash"></i>
                                        </button>

                                        <form method="POST" id="remove-{{$item->id}}" action="{{route('admin.frontend.blog.article.delete')}}">
                                            @csrf
                                            <input type="text" name="id" value={{$item->id}} hidden="true">
                                        </form>

                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-muted text-center" colspan="100%">{{ __($emptyMessage) }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- Add METHOD MODAL --}}
    <div id="addModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> @lang('Add Article')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.frontend.blog.article.store') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Title')</label>
                            <input type="text" class="form-control" placeholder="@lang('Enter Article Title')" name="title" required>
                        </div>
                        <div class="form-group">
                            <label>@lang('Language')</label>
                            <select name="lang" placeholder="Language" class="form-control">
                                <option value="en">@lang('Select a language')</option>
                                <option value="sr">Srpski</option>
                                <option value="en">English</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Description')</label>
                            <textarea name="description" class="form-control" placeholder="@lang('Enter Article Description')"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn--primary">@lang('Save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Update METHOD MODAL --}}
    <div id="editModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> @lang('Update Article')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Title')</label>
                            <input type="text" class="form-control" placeholder="@lang('Enter Article Title')" name="title" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Description')</label>
                            <textarea name="description" class="form-control" placeholder="@lang('Enter Article Description')"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn--primary">@lang('Update')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('breadcrumb-plugins')
    <a href="javascript:void(0)" class="btn btn-sm btn--primary box--shadow1 text--small addBtn"><i class="fa fa-fw fa-plus"></i>@lang('Add New')</a>
@endpush

@push('script')

    <script>
        (function ($) {
            "use strict";

            $('.addBtn').on('click', function () {
                var modal = $('#addModal');
                modal.modal('show');
            });

            $('.editBtn').on('click', function () {
                var modal = $('#editModal');
                var article = $(this).data('article');
                modal.find('form').attr('action' ,$(this).data('action'));
                modal.find('input[name=title]').val(article.title);
                modal.find('input[name=lang]').val(article.lang);
                modal.find('textarea[name=description]').val(article.description);
                modal.modal('show');
            });

        })(jQuery);

    </script>

@endpush
