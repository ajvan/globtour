@php
$start = Carbon\Carbon::parse($trip->schedule->start_from);
$end = Carbon\Carbon::parse($trip->schedule->end_at);
$diff = $start->diff($end);
$ticket = App\Models\TicketPrice::where('fleet_type_id', $trip->fleetType->id)->where('vehicle_route_id', $trip->route->id)->first();
$startCounter = $startEnd->where('id', request()->pickup)->first();
$endCounter = $startEnd->where('id', request()->destination)->first();

$startStoppage = $trip->stoppages->where('counter_id', request()->pickup)->first();
$endStoppage = $trip->stoppages->where('counter_id', request()->destination)->first();
$startTime = $startStoppage ? Carbon\Carbon::parse($startStoppage->departure) : Carbon\Carbon::parse('00:00:00');
$endTime = $endStoppage ? Carbon\Carbon::parse($endStoppage->arrival) : Carbon\Carbon::parse('00:00:00');

$date_start = '';
$date_end = '';     //povratna nije neophodna
if($endStoppage){
    //Calculates time difference between trips
    $day_diff = (strtotime($endStoppage->arrival) < strtotime($startStoppage->departure)) ? 1 : 0;
    $a=strtotime($startStoppage->departure);
    $b=strtotime($endStoppage->arrival.' + '.($day_diff*24).' Hours');
    $interval = ($b - $a) / 60;
    $traveltime_hours=date("i",$interval);
    $traveltime_min=date("s",$interval);

    //For displaying dates between trips
    $date_start = date_create($date_of_journey);
    $date_start = date_format($date_start,'d M');
    $date_end = (new DateTime($date_of_journey))->modify('+1 day');
    $date_end = $day_diff ? date_format($date_end,'d M') : $date_start; //checks if needs to show next day date on trip
}



// TODO promeniti u const = 1
$price = $trip->ticketPrice(request()->pickup, request()->destination, 'odrasli');

//Crossovers
$crossovers_count_per_trip = 0;
$trip_crossovers = $trip->stoppages->where('trip_id', $trip->id);
foreach($trip_crossovers as $item){
    if($item->crossover > 0) $crossovers_count_per_trip++;
}

@endphp
<div class="ticket-item">

    <div class="ticket-item-inner travel-time">
        <div class="bus-time">
            @if(isset($startStoppage))

                <p class="time"><span style="opacity: 0.5;">{{$date_start}} | </span>  {{ date("H:i", strtotime($startTime)) . 'h'  }}</p>
                <p class="place"><b>{{ __($startCounter->name) }}</b></p>
            @else
                <p class="time">{{ showDateTime($trip->schedule->start_from, 'h:i') }}</p>
                <p class="place">{{ __($trip->startFrom->name) }}</p>
            @endif
        </div>
        <div class="gb-devider-center bus-time">
            @if(isset($traveltime_hours))
                <p>{{ $traveltime_min > 0 ? $traveltime_hours . 'h ' . $traveltime_min . 'm' :  $traveltime_hours . 'h' }}</p>
            @endif
{{--            <i class="las la-arrow-right"></i>--}}
                <div class="gb--connect-divider "><span class="gb--connect-circle gb--connect-circle--green"></span><span class="gb--connect-line"></span><span class="gb--connect-circle gb--connect-circle--orange"></span></div>

                @if($crossovers_count_per_trip > 0)
                    <p>
                        <span class="crossover-number">
                            <a class="text-white" data-bs-toggle="collapse" data-bs-target="#collapse-{{$trip->id}}" aria-expanded="false" aria-controls="collapse-{{$trip->id}}" >{{ $crossovers_count_per_trip }}</a>
                        </span> @lang('Bus Changes')
                    </p>
                @else
                    <span style="font-size:0.8rem; letter-spacing: normal"><p>@lang('Direct')</p> </span>
                @endif
        </div>
        <div class=" bus-time">
            @if(isset($endStoppage))
                <p class="time"> <span style="opacity: 0.5;">{{$date_end}} | </span> {{ date("H:i", strtotime($endTime)) . 'h'  }}</p>
                <p class="place"><b>{{ __($endCounter->name) }}</b></p>
            @else
            <p class="time">{{ showDateTime($trip->schedule->end_at, 'h:i') }}</p>
            <p class="place">{{ __($trip->endTo->name) }}</p>
            @endif
        </div>
    </div>

    <div class="ticket-item-inner" >

        {{-- <span class="bus-info">@lang('Seat Layout - ') {{ __($trip->fleetType->seat_layout) }}</span> --}}
        <span class="ratting"><i class="las la-bus"></i>{{ __($trip->fleetType->name) }}</span>
    </div>


    <div class="ticket-item-inner book-ticket">
        @if(isset($price))
            <p class="rent mb-0">{{ __($general->cur_sym) }}{{ showAmount($price) }}</p>
        @else
            <p class="rent mb-0">{{ __($general->cur_sym) }}{{ showAmount($ticket->price) }}</p>
        @endif


        {{-- <div class="seats-left mt-2 mb-3 fs--14px">
            @if($trip->day_off)
            @lang('Off Days'): <div class="d-inline-flex flex-wrap" style="gap:5px">
                @foreach ($trip->day_off as $item)
                <span class="badge badge--primary">{{ __(showDayOff($item)) }}</span>
                @endforeach
            </div>
            @else
            @lang('Every day available')
            @endif
        </div> --}}

        {{-- <a class="btn btn--base" href="{{ route('ticket.seats', [$trip->id, slug($trip->title)]) }}">@lang('Select Seat')</a>--}}
        {{-- @guest
            <button type="button" class="btn btn--base" data-bs-toggle="modal" data-bs-target="#auth-modal">@lang('Order Ticket')</button>
        @endguest
        @auth
            <button type="button" class="btn btn--base" data-bs-toggle="modal" data-bs-target="#orderNo{{$trip->id}}">@lang('Order Ticket')</button>
        @endauth --}}
        <button type="button" class="btn btn--base" data-bs-toggle="modal" data-bs-target="#orderNo{{$trip->id}}">@lang('Order Ticket')</button>

    </div>
    @if ($trip->fleetType->facilities)
    <div class="ticket-item-footer">

        <div class="row mt-2 mb-4">
            <div class="col-12">
                @include($activeTemplate.'partials.ticket_details')
            </div>
        </div>
        <hr>
        <div class="d-flex content-justify-center">
            <span>

                @foreach ($trip->fleetType->facilities as $item)
                <span class="facilities">{{ __($item) }}</span>
                @endforeach
            </span>
        </div>
    </div>
    @endif

    @include($activeTemplate.'partials.ticket_form')
</div>

