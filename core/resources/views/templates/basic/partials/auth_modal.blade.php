{{-- @push('style')
<style></style>
@endpush --}}

<div id="auth-modal" class="modal fade" tabindex="-1" aria-labelledby="auth-modalLabel" aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Sign in to continue')</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    @guest
                    <div class="col-12">
                        <a class="sign-in mb-3" href="{{ route('user.login') }}"><i class="fas fa-sign-in-alt"></i><span class="mx-2">@lang('Sign In')</span></a>
                    </div>
                    <div class="col-12">
                        <a class="sign-up mb-3" href="{{ route('user.register') }}"><i class="fas fa-user-plus"></i><span class="mx-2">@lang('Sign Up')</span></a>
                    </div>
                    {{-- <div class="col-12 google-login">
                        <a href="{{route('google.login')}}" class="sign-in mb-3"><i class="fab fa-google"></i><span class="mx-2">@lang('Continue with Google')</span></a>
                    </div>
                    <div class="col-12 facebook-login">
                        <a href="{{route('facebook.login')}}" class="sign-in"><i class="fab fa-facebook-f"></i><span class="mx-2">@lang('Continue with Facebook')</span></a>
                    </div> --}}
                    @endguest
                    @auth
                        <div class="col-12">
                            <a href="{{ route('user.home') }}"><i class="fas fa-user-plus"></i>@lang('Dashboard')</a>
                        </div>
                    @endauth
                </div>

            </div>
        </div>
    </div>
</div>

{{-- @push('script')
<script></script>
@endpush --}}
