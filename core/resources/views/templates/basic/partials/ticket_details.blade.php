

<a class="btn btn--info px-4" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-{{$trip->id}}" aria-expanded="false" aria-controls="collapse-{{$trip->id}}">
    <span class="mx-2">@lang('Details')</span>
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
    </svg>
</a>

<div class="collapse" id="collapse-{{$trip->id}}">
    <div class="card card-body">
        <div class="row justify-content-start">

            <div class="col-12">
                <div class="bg-light text-dark p-2">
                    <div class="row justify-content-start text-info mb-3">
                        <div class="col-6 col-md-8">@lang('Stanica')</div>
                        <div class="col-3 col-md-2"><span class="sm-vertical">@lang('Dolazak')</span></div>
                        <div class="col-3 col-md-2"><span class="sm-vertical">@lang('Polazak')</span></div>
                    </div>

                    <div class="col-12 px-2 mb-3" style="border:dotted; border-width:3px; border-bottom:3px;"></div>


                    @foreach($trip->stoppages->sortBy('order') as $stoppage)
                    @if($stoppage->counter->status)

                        @if($stoppage->counter->border)
                        <div class="row justify-content-start">
                            <div class="col-7 text-warning mb-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M1 11.5a.5.5 0 0 0 .5.5h11.793l-3.147 3.146a.5.5 0 0 0 .708.708l4-4a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 11H1.5a.5.5 0 0 0-.5.5zm14-7a.5.5 0 0 1-.5.5H2.707l3.147 3.146a.5.5 0 1 1-.708.708l-4-4a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 4H14.5a.5.5 0 0 1 .5.5z"/>
                                </svg>
                                @lang('Granični prelaz')
                            </div>

                            <div class="col-5 text-warning">
                            <div class="col-12 px-2 mt-2" style="border:dotted; border-width:3px; border-bottom:3px;"></div>
                            </div>
                        </div>
                        @endif

                        <div class="row justify-content-start">
                            <div class="col-6 col-md-8 mb-1">
                                @if((isset(request()->pickup) && request()->pickup == $stoppage->counter->id)
                                || (isset(request()->destination) && request()->destination == $stoppage->counter->id))
                                <strong class="text--base">{{$stoppage->counter->name}}</strong>
                                @elseif($stoppage->crossover != 0)
                                    {{$stoppage->counter->name}}
                                    <strong class="text-bold text-primary"> <i class="fa fa-bus"></i> @lang('Bus Change') </strong>
                                @else
                                {{$stoppage->counter->name}}
                                @endif

                            </div>
                            <div class="col-3 col-md-2">

                                {{date ('H:i',strtotime($stoppage->arrival))}}
                            </div>
                            <div class="col-3 col-md-2">
                                {{date ('H:i',strtotime($stoppage->departure))}}
                            </div>
                        </div>
                    @endif
                    @endforeach


                    <div class="row justify-content-start">
                        <div class="col-12 mb-1">
                        <div class="col-12 px-2 my-2" style="border:dotted; border-width:3px; border-bottom:3px;"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>





