@push('style')
<style></style>
@endpush

<div id="orderNo{{$trip->id}}" class="modal fade" tabindex="-1" aria-labelledby="orderNo{{$trip->id}}Label" aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('Order')</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('ticket.order')}}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <input type="hidden" name="trip" value="{{$trip->id}}">
                            <input type="hidden" name="start" value="{{request()->pickup}}">
                            <input type="hidden" name="start_date" value="{{request()->date_of_journey ?? ''}}">
                            <input type="hidden" name="end" value="{{request()->destination}}">
                            <input type="hidden" name="end_date" value="{{request()->date_of_return ?? ''}}">
                        </div>

                        <div class="col-12 my-2">
                            <h5 class="text--base mb-2">{{ __($trip->title) }}</h5>
                            <p class="my-2"><span class="strong">@lang('VAŽNO OBAVEŠTENJE'):</span><br>@lang('Odabrana ruta uključuje prelazak jedne ili više graničnih prelaza pa su moguća eventualna
                                kašnjenja u dolasku zbog kontrole i povećanog prometa na graničnim prelazima. Obaveza
                                svakog putnika je da se detaljno informiše o uslovima ulaska u zemlju u koju putuje i da li
                                ispunjava sve uslove za nesmetani ulazak u istu. Niti agencija niti prevoznik ne snose
                                nikakavu odgovornost ako putnik iz nekog razloga bude vraćen sa granice')<p>
                            <p>@lang('Nošenje zaštitne maske je obavezno!')</p>
                                <hr>
                        </div>

                        <div class="col-12 my-2">
                            <h4 class="text--base">
                                @if(isset($startEnd))
                                <span class="mr-4">{{ $startEnd->where('id', request()->pickup)->first() ? $startEnd->where('id', request()->pickup)->first()->name : '' }}</span>&nbsp;&nbsp;
                                <i class="las la-arrow-right mr-4"></i>&nbsp;&nbsp;
                                <span class="mr-4">{{ $startEnd->where('id', request()->destination)->first() ? $startEnd->where('id', request()->destination)->first()->name : ''}}</span>
                                @endif
                                {{-- {{ __($trip->fleetType->name) }}&nbsp;&nbsp;-&nbsp;&nbsp; --}}
                                {{-- @foreach($startEnd as $point)<span class="mr-4">{{ $point->name }}</span>&nbsp;&nbsp;
                                @if($point->id == request()->pickup)
                                    <i class="las la-arrow-right mr-4"></i>&nbsp;&nbsp;
                                @endif
                                @endforeach --}}
                            </h4>
                            <?php

                            $startStoppage = $trip->stoppages->where('counter_id', request()->pickup)->first();

                            ?>
                            {{-- <p>@lang('Price'): <span>{{ __($general->cur_sym) }}{{ showAmount($ticket->price) }}</span><p> --}}
                            <p>@lang('Date'):&nbsp;&nbsp;{{date("d M Y", strtotime($date_of_journey ?? null ))}} {{date("H:i", strtotime($startStoppage->departure ?? null))}}h</p>
                            <p>@lang('Return'):&nbsp;&nbsp;{{date("d M Y", strtotime($date_of_return ?? null ))}}</p>

                            <hr>
                            <p>@lang('Putnici'):</p>
                        </div>

                        <div class="form-outline col-md-3 mb-3">
                            {{-- <input type="number" id="typeNumber" class="form-control" /> --}}
                            <input class="form-control" name="passangers[adults]" type="number" min="0" step="1" value="1">
                            <label class="form-label" for="typeNumber">@lang('Odrasli')</label>
                        </div>

                        <div class="form-outline col-md-3 mb-3">
                            {{-- <input type="number" id="typeNumber" class="form-control" /> --}}
                            <input class="form-control" name="passangers[students]" type="number" min="0" step="1" value="0">
                            <label class="form-label" for="typeNumber">@lang('Studenti')</label>
                        </div>

                        <div class="form-outline col-md-3 mb-3">
                            <input class="form-control" name="passangers[children]" type="number" min="0" step="1" value="0">
                            <label class="form-label" for="typeNumber">@lang('Deca')</label>
                        </div>

                        <div class="form-outline col-md-3">
                            <input class="form-control" name="passangers[retired]" type="number" min="0" step="1" value="0">
                            <label class="form-label" for="typeNumber">@lang('Retired')</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-outline col-3">
                            <input class="form-control" type="number" name="bags" min="0" step="1" value="0">
                            <label class="form-label" for="typeNumber">
                                <span class="mx-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bag-plus-fill" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M10.5 3.5a2.5 2.5 0 0 0-5 0V4h5v-.5zm1 0V4H15v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4h3.5v-.5a3.5 3.5 0 1 1 7 0zM8.5 8a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V12a.5.5 0 0 0 1 0v-1.5H10a.5.5 0 0 0 0-1H8.5V8z"/>
                                </svg>
                                </span>
                                <span>@lang('Prtljag')</span>
                            </label>
                        </div>
                        <div class="form-outline col-9">
                            <p class="small">@lang('Cena prtljaga'): 120,00 RSD /  2,00 BAM  / 1,00 € @lang('po komadu').</p>
                        </div>

                    </div>

                    <div class="row my-3">
                        <div class="col-12">
                            <p>@lang('Total'): <span class="total">...</span></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn--dark" data-bs-dismiss="modal">@lang('Close')</button> --}}
                    <button type="submit" class="btn btn--base">@lang('Order Ticket')</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('script')
<script></script>
@endpush
