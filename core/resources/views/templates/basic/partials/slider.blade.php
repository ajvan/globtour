@section('section-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
@endsection

<div class="row mt-4">
    @foreach ($accommodation->media as $image)
        <div class="item col-sm-6 col-md-2 mb-3">
            <a href="{{$image->url}}" class="fancybox" data-fancybox="gallery1">
                <img src="{{$image->url}}" alt="1" width="100%" height="100%">
            </a>
        </div>
    @endforeach
</div>

@section('section-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
@endsection
