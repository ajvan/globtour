@extends($activeTemplate.'layouts.frontend')
@section('content')
<!-- Blog Section Starts Here -->
<section class="blog-section padding-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="row justify-content-center g-4">
                    @foreach ($articles as $item)
                    <div class="col-lg-3 col-md-3 col-sm-10">
                        <div class="post-item">
                            <div class="post-thumb">
                                <img src="{{ $item->image }}" alt="{{ __(@$item->title) }}">
                            </div>
                            <div class="post-content">
                            <ul class="post-meta">
                                    <li>
                                        <span class="date"><i class="las la-calendar-check"></i>{{ showDateTime($item->created_at, 'd M Y') }}</span>
                                    </li>
                                </ul>
                                <h4 class="title"><a href="{{ route('blog.content', $item->slug) }}">{{ __(@$item->title) }}</a></h4>
                                <p>{{ __(shortDescription(strip_tags($item->description))) }}</p>

                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <ul class="pagination">

                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Blog Section Ends Here -->

@endsection
