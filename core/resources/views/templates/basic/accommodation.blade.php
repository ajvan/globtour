@extends($activeTemplate.'layouts.frontend')
@section('content')


<section class="blog-details padding-top padding-bottom">
	<div class="container">
        @if( !empty($accommodation))
		<div class="row gy-5">
			<div class="col-lg-8">
				<div class="post-thumb">
                    @if($accommodation->image)
					    <img src="{{ $accommodation->image }}" alt="{{ __(@$accommodation->name) }}">
                    @endif
				</div>

                {{-- Image Slider --}}
                @include($activeTemplate .'partials.slider')

				<div class="post-details-content">
					<div class="content-inner">
						<ul class="meta-post">
							<li>
								<i class="las la-calendar-check"></i>
								<span>{{ showDateTime($accommodation->created_at, 'd M Y') }}</span>
							</li>
						</ul>
						<h4 class="title">{{ __(@$accommodation->name) }}</h4>
						<div class="accommodation-content">@php
							//echo @$accommodation->description
						@endphp
                        {!! $accommodation->description !!}
                        </div>
						<ul class="meta-content">
							<li>
								<h5 class="title">@lang('Share On')</h5>
								<ul class="social-icons">
									<li>
										<a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}" class="facebook"><i class="lab la-facebook-f"></i></a>
									</li>
									<li>
										<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{urlencode(url()->current()) }}&amp;title=my share text&amp;summary=dit is de linkedin summary" title="@lang('Linkedin')">

                                            <i class="lab la-linkedin-in"></i>
                                        </a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
					{{-- <div class="fb-comments mt-3" data-href="{{ route('blog.details',[$blog->id,slug($accommodation->name)]) }}" data-numposts="5"></div> --}}
				</div>
			</div>
			<div class="col-lg-4 col-md-12">
				<div class="blog-sidebar">
					<div class="sidebar-item">
						<div class="latest-post-wrapper item-inner">
							<h5 class="title">@lang('Accommodations')</h5>
							@foreach($accommodations as $latest)
							<div class="lastest-post-item">

                                @if($latest->image)
                                <div class="thumb">
									<img src="{{ $latest->image }}" alt="accommodation">
								</div>
                                @endif

								<div class="content">
									<h6 class="title"><a href="{{ route('accommodation.show', $latest->slug) }}">{{ __(@$latest->name) }}</a></h6>
									<ul class="meta-post">
										<li>
											<i class="fas fa-calendar-week"></i> <span> {{ showDateTime($latest->created_at, 'd M Y') }}</span>
										</li>
									</ul>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
        @else
            <h5>@lang("There's no active accomodations.")</h5>
        @endif
	</div>
</section>



@endsection
