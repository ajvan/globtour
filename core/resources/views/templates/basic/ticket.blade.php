@extends($activeTemplate.$layout)

@push('style')
<style>
@media (max-width: 500px) {
        .sm-vertical {
        /* transform: rotate(90deg);
        transform-origin: left top 0; */

        writing-mode: vertical-rl;
        text-orientation: mixed;



    /* writing-mode: vertical-rl;
    text-orientation: upright; */
    }
}
</style>
@endpush

@section('content')

<div class="ticket-search-bar bg_img padding-top" style="background: url({{ getImage('assets/templates/basic/images/bg/inner.jpg') }}) left center;">
    <div class="container">
        <div class="bus-search-header">
            <form action="{{ route('search') }}" class="ticket-form ticket-form-two row g-3 justify-content-center">
                <div class="col-md-4 col-lg-3">
                    <div class="form--group">
                        <i class="las la-location-arrow"></i>
                        <select name="pickup" class="form--control select2">
                            <option value="">@lang('Pickup Point')</option>
                            @foreach ($counters->sortBy('name') as $counter)
                            <option value="{{ $counter->id }}" @if(request()->pickup == $counter->id) selected @endif>{{ __($counter->name) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3">
                    <div class="form--group">
                        <i class="las la-map-marker"></i>
                        <select name="destination" class="form--control select2">
                            <option value="">@lang('Dropping Point')</option>
                            @foreach ($counters->sortBy('name') as $counter)
                            <option value="{{ $counter->id }}" @if(request()->destination == $counter->id) selected @endif>{{ __($counter->name) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3">
                    <div class="form--group">
                        <i class="las la-calendar-check"></i>
                        <input type="text" name="date_of_journey" class="form--control datepicker" placeholder="@lang('Date of Journey')" autocomplete="off" value="{{ request()->date_of_journey }}">
                    </div>
                    <div class="my-2"></div>
                    <div class="form--group">
                        <i class="las la-calendar-check"></i>
                        <input type="text" name="date_of_return" class="form--control datepicker" placeholder="@lang('Return Date')" autocomplete="off" value="{{ request()->date_of_return }}">
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="form--group">
                        <button>@lang('Find Tickets')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Ticket Search Starts -->


<!-- Ticket Section Starts Here -->
<section class="ticket-section padding-bottom section-bg">
    <div class="container">
        <div class="row gy-5">
            <div class="col-lg-3">
                <form action="{{ route('search') }}" id="filterForm">
                    <div class="ticket-filter">
                        <div class="filter-header filter-item">
                            <h4 class="title mb-0">@lang('Filter')</h4>
                            <button type="reset" class="reset-button h-auto">@lang('Reset All')</button>
                        </div>
                        @if($fleetType)
                        <div class="filter-item">
{{--                            <h5 class="title">@lang('Vehicle Type')</h5>--}}
                            <div class="open-close-toggle-vehicle-type  d-flex flex-row justify-content-between">
                                <h5 class="title">@lang('Carrier')</h5>
                                <a>
                                    <i class="fa fa-angle-down"></i>
                                    <i class="hidden fa fa-angle-up	"></i>
                                </a>
                            </div>

                            <ul class="bus-type" id="filter-vehicle-type">
                                @foreach ($fleetType as $fleet)
                                <li class="custom--checkbox">
                                    <input name="fleetType[]" class="search" value="{{ $fleet->id }}" id="{{ $fleet->name }}" type="checkbox" @if (request()->fleetType)
                                    @foreach (request()->fleetType as $item)
                                    @if ($item == $fleet->id)
                                    checked
                                    @endif
                                    @endforeach
                                    @endif >
                                    <label for="{{ $fleet->name }}"><span><i class="las la-bus"></i>{{ __($fleet->name) }}</span></label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        @if ($routes)
                        <div class="filter-item">
                            <div class="open-close-toggle d-flex flex-row justify-content-between">
                                <h5 class="title">@lang('Routes')</h5>
                                <a>
                                    <i class="fa fa-angle-down"></i>
                                    <i class="hidden fa fa-angle-up	"></i>
                                </a>
                            </div>

                            <ul class="bus-type" id="filter-routes-list">
                                @foreach ($routes as $route)
                                <li class="custom--checkbox">
                                    <input name="routes[]" class="search" value="{{ $route->id }}" id="route.{{ $route->id }}" type="checkbox" @if (request()->routes)
                                    @foreach (request()->routes as $item)
                                    @if ($item == $route->id)
                                    checked
                                    @endif
                                    @endforeach
                                    @endif >
                                    <label for="route.{{ $route->id }}"><span><i class="las la-road"></i> {{ __($route->name) }} </span></label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        {{-- @if ($schedules)
                        <div class="filter-item">
                            <h5 class="title">@lang('Schedules')</h5>
                            <ul class="bus-type">
                                @foreach ($schedules as $schedule)
                                <li class="custom--checkbox">
                                    <input name="schedules[]" class="search" value="{{ $schedule->id }}" id="schedule.{{ $schedule->id }}" type="checkbox" @if (request()->schedules)
                                    @foreach (request()->schedules as $item)
                                    @if ($item == $schedule->id)
                                    checked
                                    @endif
                                    @endforeach
                                    @endif>
                                    <label for="schedule.{{ $schedule->id }}"><span><span><i class="las la-clock"></i> {{ showDateTime($schedule->start_from, 'h:i a').' - '. showDateTime($schedule->end_at, 'h:i a') }} </span></label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif --}}
                    </div>
                </form>
            </div>
            <div class="col-lg-9">
                <div class="ticket-wrapper">
                    @forelse ($trips as $trip)

                    {{-- Single ticket --}}
                    @include($activeTemplate.'partials.single_ticket')

                    @empty
                    <div class="ticket-item">
                        <h5>{{ __($emptyMessage) }}</h5>
                    </div>
                    @endforelse
                    @if ($trips->hasPages())
                    {{ paginateLinks($trips) }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

@include($activeTemplate.'partials.auth_modal')

@endsection
@push('script')
<script>
    (function($) {
        "use strict";
        $('.search').on('change', function() {
            $('#filterForm').submit();
        });

        $('.reset-button').on('click', function() {
            $('.search').attr('checked', false);
            $('#filterForm').submit();
        })
    })(jQuery)
</script>
@endpush


