
<!doctype html>
<html lang="en" itemscope itemtype="http://schema.org/WebPage" style=" zoom: 55%;" >

<head>
{{--    <!-- Required meta tags -->--}}
{{--    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>--}}
{{--    <meta charset="utf-8">--}}
    <meta name="viewport" content="width=device-width, initial-scale=1">
{{--    <title> Ticked Design </title>--}}
{{--    <link rel="stylesheet" href="<?php echo e(asset($activeTemplateTrue.'css/bootstrap.min.css')); ?>">--}}

    <style type="text/css" media="all">
        @import url("https://fonts.googleapis.com/css2?family=Georama:wght@100;300;400;500;600;700&family=Lato:wght@400;700&display=swap");

        /*@page {*/
        /*    size: A3 portrait;*/
        /*    margin: 0.5cm;*/
        /*}*/

        *, ::after, ::before {
            box-sizing: border-box;
        }


        body{
            font-family: "Georama", sans-serif;


        }

        body {
            margin: 0;
            font-family: var(--bs-font-sans-serif);
            /*font-size: 1rem;*/
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: transparent;
        }

        .header, .departure-operated, .departure-arrival{
            border-bottom: 2px solid #cac9c9;
            padding-bottom: 0.5%;
        }
        .header h5{
            color:#9e9e9e;
            text-align: right;
            font-weight: bold;
        }
        .header img{
            height: 50px;
        }

        .header{
            display: block;
        }

        .header div{
            display: inline-block;
        }
        .print-warning{
            border: 3px solid #dc3545 !important;
        }

        .grey-darker{
            color:#777777;
        }
        .grey-lighter{
            color:#9e9e9e;
        }

        .bold{
            font-weight: bold;
        }
        .gb--connect-line {
            background-color: #dedede;
            margin: 0;
            width: 0.2rem;
            height: 135px;
        }
        .gb--connect-circle {
            width: 20px;
            height: 20px;
            display: block;
            border-radius: 99px;
            margin-top: 2.5px;
            background: transparent;
            border: 3px solid #777777;
        }
        .gb--connect-circle-green {
            width: 30px;
            padding: 3px 0px 0px 6px;
            height: 30px;
            display: block;
            border-radius: 99px;
            background: #7ed321;

        }
        .gb--route-details .gb--connect-divider_vertical {
            -ms-flex-direction: column;
            flex-direction: column;
            padding: 1rem 0.5rem;
        }
        .gb--connect-divider {
            display: flex;
            align-items: center;
            left: 0;
            top: 0;
            position: relative;
            flex-direction: column;
        }

        .table-passingers tr th{
            color:#777777;
            /*font-size:20px;*/
            padding-bottom: 1%;
        }

        .table-passingers tr td{
            /*font-size:20px;*/
            color:#9e9e9e;
            padding-top: 1.5%;
        }

        /**/

        .row {
            --bs-gutter-x: 1.5rem;
            --bs-gutter-y: 0;
            display: flex;
            flex-wrap: wrap;
            margin-top: calc(var(--bs-gutter-y) * -1);
            margin-right: calc(var(--bs-gutter-x)/ -2);
            margin-left: calc(var(--bs-gutter-x)/ -2);
        }

        .col-4 {
            flex: 0 0 auto;
            width: 33.3333333333%;
        }

        .col-12 {
            flex: 0 0 auto;
            width: 100%;
        }

        .bold {
            font-weight: bold;
        }

        .text-danger {
            color: #dc3545!important;
        }

        .text-center {
            text-align: center!important;
        }
        .pt-2 {
            padding-top: 0.5rem!important;
        }

        .mt-3 {
            margin-top: 1rem!important;
        }

        .grey-darker {
            color: #777777;
        }

        .print-warning {
            border: 3px solid #dc3545 !important;
        }

        .justify-content-between {
            justify-content: space-between!important;
        }
        .flex-row {
            flex-direction: row!important;
        }
        .d-flex {
            display: flex!important;
        }

        .grey-lighter {
            color: #9e9e9e;
        }
        .pt-3 {
            padding-top: 1rem!important;
        }
        .pb-3 {
            padding-top: 1rem!important;
        }
        p {
            margin-top: 0;
            margin-bottom: 1rem;
        }

        img, svg {
            vertical-align: middle;
        }

        .col-9 {
            flex: 0 0 auto;
            width: 75%;
        }

        .col-1 {
            flex: 0 0 auto;
            width: 8.3333333333%;
        }

        .col-2 {
            flex: 0 0 auto;
            width: 16.6666666667%;
        }

        .col-8 {
            flex: 0 0 auto;
            width: 66.6666666667%;
        }
        .col-3 {
            flex: 0 0 auto;
            width: 25%;
        }

        .text-start {
            text-align: left!important;
        }

        .text-end {
            text-align: right!important;
        }

        table {
            caption-side: bottom;
            border-collapse: collapse;
        }

        .table>tbody {
            vertical-align: inherit;
        }

        tbody, td, tfoot, th, thead, tr {
            border-color: inherit;
            border-style: solid;
            border-width: 0;
        }

        .table {
            --bs-table-bg: transparent;
            --bs-table-striped-color: #212529;
            --bs-table-striped-bg: rgba(0, 0, 0, 0.05);
            --bs-table-active-color: #212529;
            --bs-table-active-bg: rgba(0, 0, 0, 0.1);
            --bs-table-hover-color: #212529;
            --bs-table-hover-bg: rgba(0, 0, 0, 0.075);
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            vertical-align: top;
            border-color: #dee2e6;
        }

        .table>:not(caption)>*>* {
            padding: 0.5rem 0.5rem;
            background-color: var(--bs-table-bg);
            border-bottom-width: 1px;
            box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
        }

        th {
            text-align: inherit;
            text-align: -webkit-match-parent;
        }

        .pb-2{
            padding-bottom: 0.5rem!important;

        }


    </style>
    @yield('section-styles')
</head>

<body>
<div class="overlay"></div>
@stack('fbComment')

{{--CONTENT--}}

<section class=" ">
{{--    <div class="container">--}}

        <div class=" header " style="display: block;">
            <div class="col-3" style="display: inline-block;">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAdAAAACpCAYAAACbB2y9AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAgAElEQVR4nO2de5RU1Z3vv9Ugb9pGQBSKbaEOPtCmSSZZeTB0EaLJxKuAuZo7czPQJHFmYlYQxoxenQQCxhCTmcVjrsmsFXO76XjNMg8R8jKQhKJjjGO8Q9O+AF/FsUBDVNoCeXV3nfvH2bvq1OlT51W/c+pU9++zVtN01al9HnXO/u7fY/92Qtd1MAzDMAzjj4ZaHwDDMAzD1CMsoAzDMAwTABZQhmEYhgkACyhTFfd9YYq479apotbHwTAMEzUJTiJinGj/6oRV54zEvKlNAy3QE5gwBs3QG6AXEoDegIljdKDQgOPvNkDXG4BCAv0DDSdPnU681NefONF7ouGlg0cSe+99KLep1ufCMAxDCQso48iGL547f+a0gVtmTu3/+NxL+s+fOBZAQQpooQF6oQGQP8b/E8XXzL+fejGBYycST2T/nHg8fwo/XfPgocdrfW4MwzDVwALKeGb9rU03z5hcaE1OKcwfPxrNV84soHGMWTBNoqo32L9eaMDhtxJ4NocnDr2pP/7umcT9//LgK1qtz41hGMYvLKBMIP79S5MePn6y4cqLLyhcdU1Lv0VI7YXT7r0XjgAvH9V3vnRU/92/PPTS12p9XgzDMF5hAWUCseVLkz43c7J+0+SJ+vw5ojBusIAmHIXTTmyffKVw8o139McOHh14+J6fvPjDWp8jwzCMEyygTNX886fP/9WsaYUFN394YEzjGFQQzsrxUegJ6AOG2xeFBuRPJfDM4YGjr7w58OO33y3c9+Uf72cXL8MwsYMFlKmKr39+yvzLk4WvTJ6A+R+8bGCcOfbpKpw2r1vf2/9GP155s3/ngaP993x1+35OPGIYJjawgDJkfOPz53/5Ly7U/2ryeMz/4GyMc3PZesviTUAfAI70DuC5N84+8dyf+u786k8PsJAyDFNzWECZUPjG31/w5VEjEp9fOAfTr7iwQqauVyt1AMAAoA/oONI7gCdeO9N5yw+eX17rc2QYZnjDlYiYUMifSlwze7o+/YoZOg6/rV4tDdYSxX8AJHTj/4N+9NLG8md64wh88vJxy3Z85uqX1193OVdAYhimZrCAMqHw9e+/3vr6scS3H3mqARPH6fYiCb0kkoMwiacN85OjL/6IGP3c/156xSeoj51hGMYLLKBMmPx88sTCK41jSy+U9NBJOPWybct+J4CEFOArJ58z4aopozau//hlbIkyDBM5HANlQuefbpqxb/aFaF4+3ynW6RQfhYyD6qZ4aOn3b7Knn7j5B89+uManyTDMMIMtUCZ0Fl2to20BYLhsYR/rNLl4E3ZxUAC2/twE8P4LRn3ovusu/1y4Z8EwDFMOCygTOj0art/2NB55/CCOPvWy1eNhSSwqe31wEtHgOCowcVQCl5478othHDvDMEwlWECZ0LnrgSNa9s9onDQe495/iZLJwbHOEv7DCu+ZOqr57kWzb67iMBmGYXzBAspEwkAB635/EN9++Em98w8vFU7ab2WyOoGKGbjF90w/E0clcOH4Ea1kB8wwDOMCCygTCV9uP/z4bd/J3XnsJH43J5kYVx7rrDTNBfavVxDWSaMbLg37PBiGYRQsoEyk9J7U892H9Ffyp9Qr7tNZgApTWky/kQDGjkxcQHekDMMwzrCAMpGy7vu5Hx56E481jrW4a4vYC2fxD9ufhKO3l2EYJgxYQJnIeftd/b6OxwsHX3jdJSO3zMXr3u5bpwvddEfJMAzjzMhaHwAz/OgvQCyak5g+sykBvQBTDDRRblnqkP+Y1bP4xiBL9PWTAy9GdxYMwwx32AJlIuecEVjRfQhvbv39AKxu3MFzQU1vOCQSHTjWDwCd5AfLMAxTAbZAmciZNB4/SU3VP3H93BGGBYoyuxKlOKj3yObxvgLW/uqApv6+o3X2Kl3XjWYKRnMjCjjyjSdf/CHJSTAMM+zhWrhMpNzTlpx/dTLxq2vnNIx74XACM84dgYmjTLVwdbsauQ3AgHT3ynVBrfVwj+T78bOXTz8xY9yI6eePbki9d9KoQfVz8ycLeO5Y31Ht3YENK5/Yv6m2V4JhmHqHBZSJnLV/l7z5xKnEZ1sva7j2o1eMdF9QW/1tWljbKqCDCs1bC9D3A3qfDv0scPxUAT/+06lb7v7jwQcA4M4rL715VCHx5D37X9QqHjTDMIwFduEykTOtMfF3t36k4dqJoxuQP6lj4qhK7try1xOmV3QYOUd6Qv1OIJHQS39bt1fTXRI6Jo5swEWjR2z+6YfnfBf9OmafMzKbPTPAVYwYhvEFW6BMpGz4TFI0jUvsGj0yMWXK+MR5H71iBNmyZvav68bn+gG9D4C0QvWzxu9dx07vbHt6/8dqeEkYhqlTOAuXiZS7/k9OGzlC/851c3HeR+ckyksKVbOsmfkt25KAg///k97TGRZPhmGCwgLKRM7URtzUOA7In5YvJABPy5qpN12WN6tEwrTNznfO7Fz5x/0Lg54DwzAMCygTOXuz2Pxvv9CfNf6qVNIPg18PUq/PxhJ99t2+g6+cHrglQGsMwzBFOImIiZzpk3Bi0ZWJqxrH6kBhcJKQ73mgjhZpeeWiZ4/3HXz8nbPXrP/jQc64ZRimKtgCZSLn9V789eFjJVEbFOu0FcNKr3vf7+/fPvvK746dvWbdkyyeDMNUD2fhMpGx4XPTRWoKNr7v4sSNM5qcM2+rmgtaKH/9+OkCdh053fmPj72wvKYXgGGYIQW7cJlIeOD2C1ZdOxf3zJmRmGCIpfldp3mgxnvOc0Atc0FN2z3f2/fGk0fPfOV//frAA6GeIMMwww4WUCZUvvEP08SVSf0Hi9+vf2jiaMOadI91DhZOAL7ctsf7dGQOn35EOz6weu1vDrDLlmEYclhAmdD47h1TP/HRuYXvXS1wAQoNgK4qAsF++TK313Wz2FZe1uy5Y31vPPn62a/c+cv9bHUyDBMaLKBMKDxw5+T7F7UUbk1OghG7LFvf09nqBOxWZ5HbuyQRPfLyqaeOHB+4ybwyC8MwTBiwgDKkfP3zk+fPu2Tg/uUfHWguJgEVca95W+2yZu+cKWRYPBmGiQIWUIaEb66cJC48r3DP/1g4cOPMyfoEDBJOH7FOyJJ+sHHlKmws0SPvDuB4n/5T4lNjGIaxhQWUqYr1tzbdfPnM/k997H2Fa+eIgQnF4u/FLQLEOq3Lqah2zG3ZcOTdAax77MDj4ZwpwzBMOSygjC82rGyaP6JBT8+6sH/e+U0D81d+su/8iWNRWjWlTAhNYlikUqzTyV3rsqwZjOSkd/v0HspzZRiGcYIFdBjxrXtmifFj9S/MOF9vGTe6cMEIFCaMGjFwwcmThZdQKAAF3fjRC9B147cxX1PHmTOYPuXcgSn/89p+JKcOyIxYSZnFaWzvP9bpPA+07DNVVCNiGIahggV0GNE/AAEA757EG++ewBtNE3FpfyJxAgAmjk80o1BA47gCrkz1QS8KasEQS/NPRQLEOq3bF2eplNy1qlhC0bKtoJin+vU3PF4KhmGYqmEBHUbc9dVXHwfgGCP81h1TxcOPnXMjoGPapIELz51QuAAFHZMmFi4d2aBPGDe6cOmHrz4zrvxTFYQzyHxPXbp+davYqv1YMolMNXKPnSm85OuCMAzDVAELKFPGP3/zzxqATU7b3H9X433XfeDUrWJa/wQA9i5VS+EDa6zz8NsJ5N4Ccm/pmNGk4/0p9Z67K9fOdXv8bAGvnxjY4/uEGYZhAsLF5JlA/Oi+8T/5ZOvJGwEAeqKs+Hvu6Ag8+fyo5/v6Gp6GnoCuJwC9AXoBONvXcCJ7NPHiyIbEkXWdh3+4YcVMkb48sWtucsRs1YZtEXnr65ai8k9oZ45e991nptX2qjAMM5xgC5QJxJk+I3Zajo7H/jjqiZ6Xz7nz7u+85XE6iY7/fDnRffKMnvzgLIwLWurvhbf6O64jOTOGYRhv8HqgjG++uepc0XxJ342lV3TkTybwg9+O7fxvX8p/2Lt4Ane157Tzz8XIOTMS4wyXrHLX2mH/+gtv9p042a/f7/kEGIZhCGABZXwzZ1bfD666uG9C8YUE8Mjvxn7702vzvtfb3PCZpHjlqD7b+MtJOHW1q0G//3D47LfX7NjP5fsYhokUduEyvmhf17hq6V+d/ZD5tR2/H7vzs+vzXwjS3szJibYPXJy4qnGsDhScC8yXIV23v9POvHL7j164M8i+GYZhqoEtUMYz31zdJFr+4uydjeNLq2HvenpMz5LbT34saJvPH9avyJ9GhVhn6e+EtcIRgCP5AWjvDHwx8AkxDMNUAQso45nZov+7cy/tu0D9vffFUQf3vTTq+qDtbfjsDHH9e/CeK6abXy2PgZbbniYXbwLYnT397S889Pwvgu6fYRimGlhAGU+033PuqvS8s9cCABLA3oOjDv7m/4255o5N71QVe9zzAh7/0X/qp42/nISzPD6668XTT3zh+88HchszDMNQUHcxUCGSTQBaADQBmOfhI68CyALIalouG96RDV2+t77pE4v+8sw9jeMLgA5ofxp5Yucfx3z2ri29VYnnXd87rG34TPLVxnEoeC0kjwTQ80bfwf1/7vubavbNMAxTLbEWUCmWaRhCmYYhnI1VtAcAXQC6AewGkNG0XG+1x0mJEMkUgBUETb2qabmOahv55pcmi0Xv7d8ophUmQAf2HjznYPeLo1bftaWXZNmw8ybq/3XljETBHOvUrau5mGKhuWMDJ555/ezqL/+ouqxbIZJpAAurPwPs1rRchqAdhmHqjNgJqBSQJQCWAlgQwi4WyJ+Vcn87AGwD8GhMxHQJgDUE7ewA0FFNA9/65yli0fvO7po3u282CtJt+/Soa+7YWJ3lqfjaihnzPz4XD8+cjAnKBjWwt0aPny7gl/vPrL79wQMUcc/VAG4gaOcYgAxBOwzD1BmxiYEKkWwTIrkHhst1I8IRTztuANAO4JAQya1SwGuJF7e0F7qr+fC37pgqFr2vb9e8y/pnA8Cup0b3/Obp0WTiCQDnn6s3vpAzD+IqFVHQcfx0Ab8+eHbt7Q8eeIBo9y1E7VR1nRmGqV9qaoFKF+1qAKtQhWuWiEYAywAsEyLZCWBtjWKmVB373mo+/N4r+3fPu6L/4neOJ7DrD2Meuen2dz4ZeK6KDV/+9PRVk8bjpr7iHegcA/3ti32PLP+PA+sp9i3vO0HRFrtvGWb4UhMLVIhkkxDJdQAOwXBX1lo8rSwD8KoQyc2ys42SZqJ2AltG//ffpm5d+L7+i7v+65yj3//5mFtuuv2dTxIdU5GvPXhk0/Tz8MZnP6KPsc75LJvvmQB+8Vzfzk/ff4DyGKgGKVz9iGGGMZELqBDJVYivcFpZCWCfTDgJHcL95Kuxnl9/MzH/oV+O7mxd3jvti/e+TeUyLeOhuy5cs+jqwo3275bcuD9/tm/npzYfpDR+AZrkIYDdtwwzrInMhStEsgXAVtBZWFEhAOwWIrle03JrQ95Xiqidqjr2L204egnRcdjy0N3T1lwzV183cXQDUDC/U166b/eBgZ5PbXyJWjwBjn8yDENAJBaodNfuRf2Jp5k1QiS3h+zSpUogyhC1EwrPvtaw6IWc9dYrL6Kw77XCwWdyA4GrHLmQImpnN1E7DMPUIaFaoDKjdTvqWzjN3ABgjxDJ1pCmvMQigShM7r1l2vzPLCq8Z+Z5crFsm/me+3KFg5mD+jV3P5gNK8ZY8zgzwzD1T2gCKl22exD/OKdfmhGeiFJN3Yltx/6eSwr3z5yiTzBWXinPvE0A6Nb0g7v369fc3XkoFPEkjDNrMZk3TIoQyTYAswia4gITzJAnFAGVD2F7GG3HhGYYg4O5VA3KAQcFVSUQhcm9f3/+/IunJZrzp3Q0ji6Jp5LS3z5f6NmnJa6/e6sWZnYrxz+dWQ0aC53d28yQhzwGOgzEU9EsRHIrYXtDvmMf0QBt/Bj9icax5YtjA8DP9uo7r1v/2ty7O0IVTyAmhSpiDJV7O0vUDsPEFlIBHUbiqVgmz5mCId2x3/sPU+dPPVf//VUXDXwoYZnv+bO92HnThlwY2bZ2DPk4c1DiMo2KYeoFMgEdhuKp2ExU/m9Id+ynzib+sm8A3cnJpWzb/Ckd255KPPLf7z0clXgCnEDkRIqonaF4bRhmECQCOozFEzCSpChcuUPahTtxrP7IBy4bSBt/6cifAnbta1j7NxteJ69yVAm2sFwZFtOoGIaKqgVUJr9sJjiWemZBNZ2ztGBJspU1LRdLAU1NK2ycIwYmADpeezNxYld3w9q/3fAGSW1bHwzpQQoBQ9oLwjDUVCWgsqjAdgy9qSpBqMYKpeq4uojaIeXrn58y//2X9d+IhI7X3mw48dtnRnzqb7/+p6jFE6CZngEMXQuLahpVlqgdhok11VqgW0G0qsUQQAiRXBLws0M6gah5Vv99ySkD2PvKiIO/fWbEp27516MU63kGgS2sChBOo4qtF4RhqAk8D1TGPSkWJPaKBmPkvxtAttIkbWkVt8ifhYj2GFcDeDTA59JE+49dx77ln8773Kc/0vehX3eP6ul5eeT1d37nz7VcwYQtrMqkiNqJpReEYcIgkIBKkYoq7tkJYKPXUa2sDpORP5vksbbBELewreUFQiRTARJMqEb/fvcbOudPKqze03POzqV3H/vYX9fwONjCcmVIe0EYJgyCunC3Ivy4ZyeAWZqWW15Nh6VpuV5Ny23StNxFANYDyJMdoT2r/WxMnECUoWiHinv+8bx/B/Dw0rt7o5ymUokhHWcmIE3UTuy8IAwTFr4tUJltGqZbtAdAVaJZCU3LrRUiuQ3hLquW9rl9imi/PUTtkPGV/3j7i7U+BhNsYTkzZL0gDBMWQVy468iPokSnpuWWh9g+NC3XLUSyFUYt2zBEtNmnG7fuFnc2xZnVsbcAsC7z1ms6pt0AumtcfD2yBCLpLm6BkfVrd20yAI4ByMTBHTyUvSDy3FIo3atpm83UvXpM/q71vcpEgLw3WlB5cP0qjAFhxfvBl4DKxCGqRAwrKzQt1xFS22VoWq5Xiug+hBMXTQPo8LhtXWSGygzjpTDOzes1U56KNbINDUaSVXsNhCPUlW6kaK4AsATu16d4LEIk8zCuyTZNywVJQKuIjznaZGvcCpHc4/Mj7ZTPvRzcLYEhmGn4v1dVOz0wBjqR36sBrqEtmpZrJTgWqnn+vZqWW+xhf5tB0yfeZve9SdFcDW/PqWIpKiSH+rVAfcX3fBCZeCqkiC5GOMIzD9ELKPlDLh8edbNRWCgCwEoAK6WYbgTQEfZon6jUIoDBCUQypLEOwQW6EcAyGHWVNQBrCZ+FdBXHFRS/+9tGsVP5PayAcS0paJY/K6WYboyij5LnQfGdUcXqWxDt8bSBpq8p61OqfE4r9q2ek4jkAYTh8oxcPBWyM+wMoWlPoihHyyQWMKXrTIhkWo6C98LokMJIGBMwBPSQEMlVIbRvhjyBSIhkkxwt7wadSAkA7UIk9xCJPlXcN0yqGviZ7tXdoBNPK80wvpdDhItHVCJuA+rIyjsShhKKpTYJnlPHsp1+snDDsD631Eo8TWwMoc2Ux+2oHhaSBCIhkikhkttBKwpuNALYKERyH+VUEwtUnUAWKFrm+2BY02GwAMC+KgpzKFIExxIqQQd+QiRbTMIZ1b1KPcCxg+pepfKsRRliShHtqxsge04dByKeBFTeLNSZtz2alruNuE3fSCuUenK/V6uSKoEoW20DQiTXwbjZoiw8YaYZwJ6QRvhponb2SlHbg/DnFDcC2Fbl9YjafeuXQAM/ea/uRe3OTw1w0iG0HTcLNNTcAQtkCZXyuaF4TjNOb3qNga6o8iDsCDXb1idrQVcn1Q81f1hM9Yzj0Nk2whjhz9K03FrCdqmu8zyE47Fwol2IJPx6akK05inJ+tlYnlOYU9D80AhgtxBJ6hAUyblRJD4R3kNeVy+i2l8L6LxDjpazVwFtq/44ylgfh/R9RQ3dyFQ3zO4gH5IPyB7EbzGANXIqUNWDLMopGggvxuZGEBGtBwH13AdIyz+KAi5+CTTAsYPQoqWaEx71AJ9qf5TGQHUuXNnJUrqr8oh+FB9XqK6r78GIdHHsRfw6JMUyouSiehASL/hduL0eEog8DfzkvboN8b1X24lCDzX3SFmIrPgIZUIlIa6Ws5cY6FKaYymyiScpk442Nb/Xs44WQN9IkEhTD0LiBb8Lt9fDwMFLx9qG+rhX2wlcnsM5gSiO96vr/elFQKvtwKzUw8MQBTUZbdZRh6TYWmXGYxwfzKAs8GHpxP28XQd+dXivbpeWVFBSRMcxnBOIKMm4beAYA5U3A2XAfkeAlUoGIR+sMBKbqOj2kGEceW1WOUKupw4JKFleQauqxF1I/LIOLkU6iOO+YeF439aheAKGC3IzgidIkggWxZzwMIuPVCCOz6mr5eyWRER9UiRVR2C4leOQNVoNkSYQyQeCpERYDVggRLItQCZqHOMq1SI8XIs4dkZWKnaqhOXjasEyIZLtfkWMMOOVakoeefERF1JE+6OkahcutVlNVeuzHjoIN6gse68W6HbE3ypxIsgiBkPhPrHDzftSD3Ff29G9aVoV36vBqLsEIkkcpiaZ8TT1xk1AKTugHorkoaFgVRAmEOW9XFM58TxuN6hfRIBMxzjGVShY4OJiq4eBQ6WOdR3q/PmG8f2kfX4mbsvtpYna8bJ6EdW+KPF0Hd0ENFX9cRTJELVTD51DxuX9yEabsqNdQ7S/WuM37l0P90pQnJL74n7etqN72ZGGVR4xamp1rwaaE25DlBZxHO/XjJeN3ASU0mp5laidoWBVUFU9ynjYxs/Uh7jjZnlZieODSYXtc1AnHppKnWq9xj3tWOYzIzfKjFdHiNeH9XI8cQw5eNKriklEVaZj20HlWqiHTtHNbRHJ/CrCpZEqocEox5aRfzeBbvmjSiwBsMlto4iEpAdGXH8vypdPWgjjOoRZVzhd4fV6eD4y1hekez7MMIMGuVi2/DsF41qFuc80POR9EJfMo5hjH3UCUZT3bB7GPZBFqZSk6rdSKPUZnvTKKQs3rg9iXI/LjNtNHNVoM0gygxt5GNMoKi40LMVrNYBVoE8GWQgPAopw75MuGOt2Ziq8nwGKI/nNCEdIG4VItth8Byl467haQPPdqEGUH+zcjGHdq5tg3KtZuw1MCyyH4TpeCG+Jkymi/cUtgSjrcbuw8zNc+yyF7Lvsnitb/C6oHRiiuUn14J4C3FP0KXDMEgvJ+twBY6X3ivsFjMXKAawVIrkN9MW/0x63C0NA8zDOv8PLxvI6LQ5xTuMgL5E8tg63D8qlwCjuj+XVPtvy+lA/1ztgHJvjYFZ+R7cJkWwHfV1or/dgZGtueiRN1E6tE4jUAGqjV8tcbpfxugM/64HGgXqwPt3cKFEF56nXb12tabnFfgphyFFcK+iKWwOG5eUlvEAdV8kDaA1SNFx+JozCH9XkA8Qm5gb6e3WFvFc9uzNN92qe8Di8XuM00f6oSviliNqpZQJRF4C5mpZbG2bpWCcBpY6BUlAPCURuN03oo00pMJRuwxWalvPiNh2EvHkXg7Zj8vLQUT6YSjwDi4UU0R1kR1QFcYq5yWOh9FAEXl7MJKJkeEx6i80cUEovn0fPRBjLSHZqWq6VouqdG04CGsfMqHqwQDMu71Odg1OWWBvRPgBgS7VLNckbOZAAVwFlp3wb0fJ7y0E7kAhKbDps0Frmqwnu1W4AnTSHA8DFmiPOeM0SNEN1b3j1OlH36Z0UyyB6JTIXLlFtxXoQUDc3ShSuMypLvcdDTV+vbASdeDjeB8RxlR1U68VKa42qGlc1xCnmliZoAwC6gnpJbKBczN2NqDNe3aDqO7wOrijzNLqiFE/AWUCp/OmKVDUfrqMEokylNyIs0EzlvqUST9/BeRcmubxPOdAiuwYSqnrQ1RDJNCo35PNA5Skg6zilJUcZt3cibhWIIrs3CEMJgDE4j1Q8AWcBpQ68Vnux6sH6dCtXGPpok9D66qLInLZA9ZC7QdUpdVHHUTQtFwcLNC4JRGmKg4DhtssStaWI6ntKE7UTtzVAo04g2hRFzNNKZNNYQNOprSdowwx1iTu3hy6K0SbVTRnGtIvdiKasYIqonXpbTssVQi+Ip2LbLlAlkMTBqg9Kiqidekwgosyz2UjYlmecBDRLvK90NR+WX0iG4kCAoqVG3Zm7Pchpov04jTbjFN8KizjEmeuVOCUQpQnaiItV7xtiwaL4Puo1gagzzKkqTlR04YZgDgtin3e1UM/L0zzcxFF0XimC9imsCzuozr/iw0J5jxF1SmESpHB4nAZYKYI2qJJnakHUguUGVQJR1uN2VAPdmnkg3LJwqQPp1BOmAyFHfkuIm3UcBUdYoJli/m5YwuGW/OOVKOLMoUw3oUwkQ7A8hTTRvilibnFOCkxRNOLiyow649WNyLwTxKGEmnkg3ASUuiNdRtyBBGU16Gu0uvngoxptxnndzzRFIy4DiLhlNVqptYVMtf8sUTtxJU3QhtsgLBbZ0CaojseLZyROoYTAuAko9VQWoMZLFkkXH3Xs00u2Ztw79lCRAycKl43bAILqwQxrRZmlRO349g7VYJmqukT2ERTWcRxCOp4gPGfA2/HEKZQQGDcBDcM0vkGI5KoQ2nVFum63h9C0lwywNNG+whjUWAkjVk0Vc3Z7OMmEj3pJP+LQQS2tzzjFHcMoOUoVasq4vB8nFzbVOWseE3rSRPuLoj+siKOASqtKC2G/G0Ouwj8I2XntAf1Nq3n0waeI9hfFyL+RMhlHXnuqQVNF91AISWpp4vbaQBc6qGUCUZysz2bKsJBsaxlRc073appoH1VTo4FdbKzvavBSyi+sAO12uYxR6MiOdQ/CiQ+6lv2qwfwqCigTvraCTjic7scU0T4UZNdA3gOUa15mAnwmTbTvmkwZcIAyo57KQ5WP8Fmtls2gez69JhBR7C+s2QKe8SKgYU1QbQTQLkdjTkUAABHkSURBVEQyjIV0i0h3cVjiqXmskxplujqVx4Ak4Ut+v1SlBbtc3EPUCyAsILQUKNea7Kl1xxEzVlG424VIUq5d62Z4UK4sFTiuLu9vKosbGEYJRIAHAZUPapgxjzVCJPdRuzSESLYJkTwEYwBAnXGr8FonNcp09SzRvgDDSxC4Y5IeBsqELbfKQGnCfSk2V9s5E3fMQPAKSVTxYaq4I9VgrxGGlyMw8juiFJIo5ya2BblHpWeOOidk2CQQAd5XYwm7TFIzgN1SSAPdDIAxmhIiuVkKZzvCDdJ3+Zh/FOXUAS/beKUZwFa/34cQySbZIVGWwsvDfVQfRvJTM4A9ATuoJiGSe0DbMQO1X9GFKl6WJWoHMJITfYuo/I62g/Y78poXQUUjfM5uMIW1KI2LqBOInJZ0jARPtXA1LfeoEEkN4WeNNcPodNuFSPbAGM1kYWRaWb+YJhgjmSYYHWdY0w7s8Fv5P8r5VXtB2xncAENAbvMS05FW5zrQ3yubnB5OyikaNigRXezVdSpEcgmMTo36OpAXuA+AECK5lWDpqG7QPrfLpDB4+p7kvUoZ/1PUoi7rMiGSgLF2rdNz0gQjth9GTeqsx+1SRPuruQvXTzH55QiW+ReUZsS3KMBaHx0pWQIRvN0wGaJ9mVEegh0wXFPdah6g7LBSMNzUSxDOICuP6ApVVKIZwKtCJDsBbLSbB2nKZlyB8AZ0Ua5V6YSnDtuF3QBW0h0SgPLvaTfs79WlML6nMAZceQAdIbTrhWUA0kIkOwDsVgNeObhsgfGMtiG8gWbGbYMY1v+tioSu6543lu6oKC29ONKlablWrxvL2C7FwEPTtNxFHvf5DsJ7SGrBek3LOQqHTFaKYqUXRR7lA5oUwvfQ9Ghabm7QDwuR9P6we0e51ndjsAWSAjCr0ncnO9NjIRxTLVntZWHvGtyvUbDUzXVN2B/66ofDwu9yZssB7MPQ6pz9kAew2OdnapFx9ijo4261wov1CYSTQOREI6IfTFIv7k1BI4x7rdL91oMKVrOm5XqlV4MqS7vWaF7EU1Lz+F0IeOmj4lb/tyq8JhEBKGbkxsWFVAtaA7irajF5fSitY7nc4zUP24VbaygWOKdeHMILbvdtPa/lacVPTDgb1kHUCK9zMqme01gMQHwJKADIEVacSnlFxYoaF+/27PaQHW0YFaSiZoeXbEbpChzKXhG/SWuVyBK04RfHUmtyHvVQuFe3+BzgxMKCImRYVSBS+BZQyWIMjZveKys8FkywgyoRyu8NU++eAg3eRWOoW5+ek9ZcqEWn42WftchapaRH03K+3OvSq1ILj0BYZNw2qNOKbI4EElD55S9GSGsmxozA4klYHMLr/CrTB3IdqF9PQR7GVASv50xZ1SVudPmIq7lRC3epq4DK86vXAXmQvAhFhvA4ao2Xou5RVmSLhKAWqEohbsXQFtFqLE+g9msvxjHpxAu3+XSXD1ULVEPwznlwY8Y1jVKo/Az8KFzUUZOHkReRDfj5oZSr4OV5HVLuW6AKAQWGvIhWK55AjUtWye+Hsih8FAS57kNRQP1a4V7pIG7PCc8dnXTJbQnvUELB70CvDPnZevUSmfGaQETVH9Z0CTMzVQkoMCRFNA9gIYF4AnQVNwLfMNI91kl0HGHjWzyJC1XEBWXZhDHS3ojonlVfxy/jiPUgKHnQDLCB+s9VAIZpAhFAIKBAUUQvQox80wHpAnARYYCaap5gVTeMLLkW5+8mD2MSdkeAz1I9lHEZAIYpnip/ISrXfpAJ84sR/3u1lUg8leVdD4MGJ7zeq7VKqAwNEgEFjAdTVkmpNzcMYDwU6zUtF2Sepy2EiztTrXnXing+qBqMDilo8W2qBKIMgB1EbQVFXYtQOwjZ+Udxrr7PQz5/rYiniPYAmBvC91PvCZmuHrJaJlSGCZmAKqQbZiHqJ6uuC8ZDQe1KiZW7Qg5wWhEvd24nqu+QUkTH0g0jkaVW9626D6MaXYftlcgH7ehMIlrrAY2ZLZqWmxtGIX/T+UYJ5Xc/LBOIgBAEFDDcErJu63rEd2TVBSPWWU0WnROxXPNOunNXo7bfiwbj2nutMuQEWaGKGk7PIvV+eCECS6/asEOvpuUWw+hDakkPjHs1VLe3HDitQDT3XhcI3fgeB321qMgWOqEIqEJadXETUrNwZkLcD1XHTp5xJhOL5iJ6l65ylVPGmUnjKrIzmItoXIhdAOaF4P3whElEw/BKZCgakddmHqJ36WowEoXmRjVpX7rWWxGuF2SL9EQ1EbXntQ+JvCJbFIQqoEBxJKmEdAVqE9vQYMRmZ0UgnAqqBKIsUTtlaFouKx+khQhfSDUYg6iLKMUirLiK9Ei0Irx4fg+MpKnQ451uyOdzOYwlvig7brKBn6blumV+xQqE72JXwnkRVaKQr52XBnDU957y+ijLM2qLkGqgmyVqhwRfy5lRIRNs1Jp8Ya352QVjFLwt6k5Knh9JB6JpuQRFO25IMVoBunUS1TJX26pIEHJEiOQq0JSB2yHdhXb7SMFYILza66KuR3tcypDZIReZpljPdFZIoZEw79VYfTdE954GY/3asmpWhEtTuk7nIewP85qWO5egHTJqIqBm5Dy+NIwRUQuMpBA/oqrBGJV0w6jQ313rh0CeE4XLorcWForsoBbCOIcWeJtn2YPS97A7iu9AdjApgqaybp296T5V18Wt81HrharrEcogIizktU2j9Fw2ofJz2QOgV/5E9v0DZfdqGt7XZFX36m4AmVp7AbwgRHIJvN97PTC+h4qDVylqFG7cbrfYfb33h07UXECdcOggXTs8hpZKD0GtByu1xsaNHLuHfLgxXO5VGxHkey9iYi2gDMMwDBNXQk8iYhiGYZihCAsowzAMwwSABZRhGIZhAsACyjAMwzABYAFlGIZhmACwgDIMwzBMAEbW+gAYhhmayPmY223eUgUX9votMCHbXAKjklkTjKICxcII1oo7HtpLWdprhlHFrBdGZaJABTCESK6DUdzBjgwqFJuQx7PVwy66KxW4l/NDV6BUmKYJxvXOIkC1JVmdaqFsqwXGtcnKNtud5p6aKlvZkYVRzOJRrwspCJHcKo9jrdt5CJHcDvtiERkYRXc877cSPA+UYZhQkEUm3Ip/awBu8yJUUpRWwbmsXY9sL+PSVhOAzQCWuey2C8Byv4VbhEjug3tFtU5Zh9j8uTS8FUy3+2wKhvi6VSraAkOA3CoItcG4Rm5lBLc4iLmXkoFqHdysy/EsAbBN/rneqa62/H6Puew3D+M6+Bp0mWEBZRgmFKTgrYEhalaBTKG8xqtjTVWLIGkwaiCbLZ+lANpM7S11KGPXBGCPqb0uAO0oFSpvku0pcc3DWKs1C48IkVQd6xYYFpsiBYfzdrlmZsosWGl17jG12wlDbNS+m2BYgjfIv3tgiNYgEZXXZ6tpWw1AB8qFPYXymsm29aSFSL4jj6kTgwvBt6FUerFLLm5hizymfT62T8vjzQOwCqTyYqi2Bg1GvMICyjBMKEgX2g2oYC3YdNQLK7g1t6IkZhWtJ4sw5mGs/tPrsk1FC8QiSo4dtuVzaUixqbQYhBDJQzA68LJ23a6Zh+PUYFjMmQrbLoFxzRtRwXK0WI2rnSw0IZKbAayUf1oHAykYrlKn66AGDIDDAgSm7fLy2B0Ly5sWmqj4vVmO3fP1NsNJRAzDhIWqR2u7Eodp0Wy1xOE66zYm8cxDLsdVyfVoWt9Ug9HJbra0ZRZPZYFVFAcZ22uV+14gXZpeUOfttExgxuWznta9tIjnDhiWcqW2Ia1yZW2tlJ83t7cOJfFc6ObelAKsznO15W3VttMSllardhBSiJXIqmNvlK9XQi3Xlqm0gTx2tWzcKnl/+IIFlGEYcmRnpFxkbgXOletvgblDl5acsjwXe0l+kSKqLAlrfHM1SpbnYi+F1+U2HfLPSskwVmbJ307tp+XvdvWCz2umUDHKHk3LLfaSFCNFVIla8ZwsQrXaR7KRWlLQGvNVIuZ0LtkK/zejkqo65bGrNWGdVnhxHLyZWIuSVbvEZdtBsIAyDBMGqgPLu8UO5fuqQzd3isoi7fSTOWpxI6bl7yYYCUiA4bZ1PCYLKnHF6/qZjp23tPIEjIXcO0xvma+ZqxDKwYY6JtskHgdUfNV8vZWYaj4TazIwsnQXWl5Py99Zh88qcdTsvhP5/S2AdLfLl5UgW/dnRom540BEXmd1LXwvMs7TWBiGCQPVMXu1pJRgzAKK1pASB4pF01XiTj5A1mXW5/bquJssy92p6SXNkLFKy+eUIPRKkbXDnDykBK8rwFJtu1GyNhXKAvN1vaUI2e3f0R0t45TqWlWKPyqB3WQS2G4YcWJbC9R0zV0HbxK1je81S1lAGYYJA9cYlAtp+VsjWuNSHU+QeZ2eY2OWmGIlIao0bUN9VmCwuCmOoXRN0/L3NtstnUnZvKastozNe8qKt8Y5y1CJOHIAVMyIFiJpthZTKM9E7rTLwLZY6maBVeJfySPgd/AWGBZQhmHCwGsMSpGybK/iiFm/O7Ykg6hOVB2P7/ZQOjbNaSPLfvIY3IGnYAiCALBPiGSrZXCgPms35UNhHgB4clNWQA0oeoGi4AEoxn3taEFlYQcMN7wSupTp9ZWDNwUgp5hUyNBOoeRyt1rq3abtWmyO10vs1Yy67hmP2xdhAWUYJgw8d+7SavObPOOEckVq1VaakSyVvzMetlWdd0eFKSJpGLHdBTASgMxTLNQ18BujDYK6Rsq96sXK7gWw3ub1NhjHnjW9pixO63zWVTAsz/UANjp8Pyo5aofVPa1puV4hkprcZwsG3zN+B29p+ftVj9sXYQFlGIaUADEo5RbsIRIOFT/sqLYhaQmpbN52h00VblN3MkIk22EIaNEFGeCamfE1/ULuS4l1hzyubiGS6n07q05ZpoNeN03vMcc61XV41Gxhyn2sAZCuNO9SHp+aG+wUDwaMAUuH5TU/g7c2lFzJvt37nIXLMAw1nmNQ0vpUAmWOGSoBWuBnfp5MTBEw3IPm9tSx+J2qoOaS9nhM1FGimHXYxu69IHE7Nf/SKRu1DFPxCsCIPZotQOWiXgqPSAFSYmwWoEoJRGoQssCSYGXGXAt4GQzBtf6ofZa1YW7TLXZuKucIDL4WnmABZRiGGk8xKFMRAMAQqA7T2xnT/x0TV0zttaEkmtZqRSrRptmh47a2Z66S5DpNxJxA5CK2xekipteCJF2pc2pzKSoAoKy4vxpgWM+pQ/72VFRAbmOeapQ1vW7rkpfbdMo/7QpnFBOHYLh5K/2oNqxzT1Pyt1MRC3NRjUbYXwtPsIAyDEONawxKlpQzd2BldVSl+Kl42xqnKkBCJJuk2CnrptM6VUUKmupUt1sr8FjaS8nau8oyXuHR+kzJ3xUr71iKQ3SY3vIbt1OfV0UAtjuJntzvPpQs5EFFF6RLtUe2t8elvRSM70+JnVmA1LlUikEr122ZFWpJHLpN03JrK/2Y92cZELkO3kwDNyW+y4PGyjkGyjAMNapjss6DBMrnQgJG57u4QtxvIwyXazOAdjkVorgcl+xw0zAsGS+FwZfDEJFGAHuFSG6BaTku2bEuRfmKL45F7i0UM1ttzjuF8uLrPSh3MavrkXKwkHvNbkmZTLMchiXaDOCQEMm1MOKOWXlOablfc2H8irVyYVyjvbK9fbK9jKU9c+F+VdXJLEDKpZy124Gm5bJCJHfAsO5Xo2R1r0Op7rBjPNImkUi1URwYVfgOlqLkVVDXIsjUJgBcTJ5hGEKE9+W4AA/Lapncjm5VgDwtTSVFUrkxnXAsyl6hbS9LdwGGJVwUHR/XzLYwuqVAvNt+XZdmk9doK9yXY7Ntz0tBfMs5z4IhbsW/vSRSmfZTHDSJ0io4bgRaps4KW6AMw1DjFH/KwsciyqpAvHThLoVhcZqFoguGBdbhsb1uABeJ0kLPZsHLw7BktvmwOs30wv3ct9lYPE0un1PYZgFrWu5RIZIXwbDmlMVefBvGOXleSFteo7kyIcvsLQBK18hpsfEsjPOpOCiQ2cidKBVVmCQ/s82HqG2Dce3Mc1mdrmMvSvee1304whYowzAMwwSAk4gYhmEYJgAsoAzDMAwTABZQhmEYhgnA/wdVlXspl3WNtgAAAABJRU5ErkJggg=="  alt="<?php echo app('translator')->get('Logo'); ?>">
            </div>
            <div class="col-3" style="display: inline-block; padding-top: 2%;">
                @if(isset($ticket))
                <h5 style="">TICKET NO. {{$ticket->pnr_number}}</h5>
                @else
                    <h5 style="">UNKNOWN TICKET</h5>
                @endif
            </div>
            <div class="col-3" style="display: inline-block;float: right;">
               <h5>GLOBTOUR BUS TICKET</h5>
            </div>
        </div>

        <div class="row print-warning p3 mt-3">
            <div class="col-12">
                <h3 class="bold text-center text-danger pt-2 pb-2">THIS TICKET HAS TO BE PRINTED!</h3>
            </div>
        </div>

        <div class=" departure-operated p3 mt-3" >
            <div class="col-3" style="display: inline-block; margin-top: 2%; margin-bottom: 2%; ">
                <p style="" class="grey-darker bold">Departure operated by:</p>
            </div>
            <div class="" style="float:right; display: inline-block; margin-top: 2%; margin-bottom: 2%;">
                <p style="" class="grey-lighter">JELINAK D.O.O</p>

            </div>

        </div>

        <div class="row departure-arrival p3 mt-3 " style="height: 300px;">

            <div class="col-9 d-flex pt-4" >

                <div class="col-2" style="float: left;">
                    <p style="margin-top: 5%;" class="grey-lighter text-end">DEPARTURE:</p>
                    <p style="padding-top: 70%;" class="grey-lighter text-end">ARRIVAL:</p>

                </div>
{{--                <div class="col-1 gb--connect-divider gb--connect-divider_vertical">--}}
{{--                    <span class="gb--connect-circle-green "> <i class="fa fa-bus text-white"></i> </span>--}}
{{--                    <span class="gb--connect-line"></span>--}}
{{--                    <span class="gb--connect-circle gb--connect-circle--orange"></span>--}}
{{--                </div>--}}
                <div class="col-1" style="float: left;">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEMAAAD6CAIAAAB1bW2ZAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAFdklEQVR4nO2dMWgbVxjHL3KMDxqJE4HKdxhSyYssLUYamuDJkBp78FCXE80iaJaO8uLNzZBm6+KMWVrwVE40zeagFjw2HWS8yPJ0akHc2YWgQ07hgrDd4eAolmPffe/J/aN+v+lJvO/77ud77+m9t/jW+fm5MhYk/usHkAab4MEmeLAJHmyCB5vgwSZ4jI/J7RHl9c/6f7z7vd1veIOuf9o/8tuKokyrc+pESpucmUstfXLnUzWRkljxlvTzyZF/sPvX88P+L9f2zKc+W/y4Nq0WpNSVaeINurvHz/e9n2JFzWtfLGZq2uSMYHVpJu1+41V3wz87IcSqieSX915kP7ov8gByTN68/WHH/VYwyYr+zf27X5HDJZj89vb71+4zwSQBy/rmg7uPabGiq3C735CloSjKa/dZ5+83tFghE2/QfdXdEMkwzI9/fu0NuoRAIZPd4y3aFL8C/+xk93iLEEg3OfIP9r2X5PAr2PdeEl4L3YT2l4vIjvM0bgjRxD/rH578SouNAiE50aTzjrjCRKfdb8TqTzQ5jFnmBkoQTXqkhXKkJYgm708lL77iJYgmwXljpMQtMT5nRqJJRs3LfQ7xEkQTdULmwVVKCaJJWviIJ70E0SSfWqIFjq4E0SR7R+ikGoW5mzFRE6l88iEtNgqE5PRVeDGzTo69lhXjSdwQusm0WpjX1sjhVzCvrREujYR+GRcz61OJpEiGYaYSSdrbFjLRJmc+n/lOJMMwj+69oN3iie5W5lJLy/qmYJKQZX2TfH8nYd/14O5jKTIil12K3NvUn7sb70lXLVOJ5COQ29QAb9DdPd6Ke+Eyr60tZtaBbrhDvEF3x3ka5Uohn3y4YjwRdwiQbxLS7jcO+43eoOuf9o/9Q0VRMmpenUilJ2fyqaW4m5FrGaFJSKfTCdvZbHZEVf73Z0ZA2AQPNsGDTfBgEzzYBA82wYNN8GATPNgEDzbBg03wYBM82AQPNsGDTfBgEzzYBA82wYNN8GATPNgEDzbBg03wYBM82AQPNsGDTfBgEzzYBA82wYNN8GATPNgEDzbBg03wYBM82AQPNsGDTfBgEzzYBA82wYNN8GATPNgEDzbBg03wYBM82AQPNsGDTfBgEzzYBA82wYNN8GATPNgEDzbBg03wYBM82AQPNsGDTfBgEzzYBA82wYNN8GATPNgEDzbBg03wYBM82AQPNsFjfExu4v/93gzj807Gx+T2SLO3Wi3btl3XtW07+CaXy+m6Pjs7WygU5NYa1Tyxbbter/d6vQ91SKfTpmnmcjlZFUdiYlnW3t5elJ7lctk0TSlF5Ztsb28fHBwEbVVVy+WyYRi6rhuG4TiO67qO4zSbTd/3gz6FQqFarYrXlWzy77eh63q1Wk2n08PdHMep1+uu6wYfpbwZmWuXbduhRqlUqtVql2ooimIYRq1WK5VKwcdmsxkuCWRkmliWFTR0Xa9UKtf2r1Qquq4H7Xq9Llhdmkmr1fI8T1EUVVWjj3vTNFVVVRSl1+u1Wi2RB5BmEg6Pcrn8oUE1jGEY5XL5QgYa0kwcxwkahmHECgz7hxloSDPpdDpBIxz6EQn7h0sZDfn7LvI7CX9haMg3iTtIwv7B1CcjzSSbzQaNuIMk7B93WF5Amgl54pKXigtIMwl3tc1mM7pMsAe7kIGGNJNisahpmqIovu9H/8Gu1+vBRNc0rVgsijyAzBkf7lBc140iY1lWOEmi7G6uZrR7YdM0Lx39F/bCCwsLq6urgqX5fHIl0c+MpVJJfFwFjPAcb1lWsDu+FE3TKpUK+jk+JLhbCQaV7/uqqgbDLJfLCa5Uw/AdJB5sggeb4MEmeLAJHmyCB5vgwSZ4sAkebIIHm+AxPib/AFnZ9hXD+AegAAAAAElFTkSuQmCC
" style="height: 175px;"/>
                </div>

                @if(isset($ticket))
                <div class="col-8" style="float: left;">

                    @php
                        $startStoppage = $ticket->trip->stoppages->where('counter_id', $ticket->pickup_point)->first();
                        $endStoppage = $ticket->trip->stoppages->where('counter_id', $ticket->dropping_point)->first();
                        $startTime = $startStoppage ? Carbon\Carbon::parse($startStoppage->departure) : Carbon\Carbon::parse('00:00:00');
                        $endTime = $endStoppage ? Carbon\Carbon::parse($endStoppage->arrival) : Carbon\Carbon::parse('00:00:00');

                        $startCounter = \App\Models\Counter::find($ticket->pickup_point);
                        $endCounter = \App\Models\Counter::find($ticket->dropping_point);

                        $date_start = '';
                        $date_end = '';
                        if($endStoppage){
                            //Calculates time difference between trips
                            $day_diff = (strtotime($endStoppage->arrival) < strtotime($startStoppage->departure)) ? 1 : 0;
                            $a=strtotime($startStoppage->departure);
                            $b=strtotime($endStoppage->arrival.' + '.($day_diff*24).' Hours');
                            $interval = ($b - $a) / 60;
                            $traveltime_hours=date("i",$interval);
                            $traveltime_min=date("s",$interval);

                            //For displaying dates between trips
                            $date_start = date_create($ticket->date_of_journey);
                            $date_start = date_format($date_start,'d M');
                            $date_end = (new DateTime($ticket->date_of_journey))->modify('+1 day');
                            $date_end = $day_diff ? date_format($date_end,'d M') : $date_start; //checks if needs to show next day date on trip
                        }


                    @endphp

                    <div class="departure-text">
                        @if(isset($date_start) && isset($startTime) && isset($startCounter))
                            <p style="font-size: 15pt;" class="bold grey-darker text-start">{{$date_start}} | </span>  {{ date("H:i", strtotime($startTime)) . 'h'  }} | {{ __($startCounter->name) }}</p>
                            {{-- <p style="font-size: 13pt; margin-top: -3%; " class="grey-lighter text-start">&nbsp;</p> --}}
                            <p style="font-size: 10pt; margin-top: -3%;" class="grey-lighter text-start">PLATFORM NR.: </p>
                        @else
                            <p style="font-size: 15pt; margin-top: 8%;" class="bold grey-darker text-start">@lang('Unknown departure time and place')</p>
                            <p style="font-size: 13pt; margin-top: -3%; " class="grey-lighter text-start">@lang('Unknown day')</p>
                            <p style="font-size: 10pt; margin-top: -3%;" class="grey-lighter text-start">PLATFORM NR.: NOT FIXED, PLEASE CONTACT OUR HELPDESK</p>
                        @endif
                    </div>

                    <div class="arrival-text" style="margin-top:20px; padding-top:60px;">
                        @if(isset($date_end) && isset($endTime) && isset($endCounter))
                            <p style="font-size: 15pt;" class="bold grey-darker text-start">{{$date_end}} | </span>  {{ date("H:i", strtotime($endTime)) . 'h'  }} | {{ __($endCounter->name) }}</p>
                            {{-- <p style="font-size: 13pt; margin-top: -3%; " class="grey-lighter text-start">&nbsp;</p> --}}
                            <p style="font-size: 10pt; margin-top: -3%;" class="grey-lighter text-start">PLATFORM NR.: </p>
                        @else
                            <p style="font-size: 15pt; margin-top: 8%;" class="bold grey-darker text-start">@lang('Unknown arrival time and place')</p>
                            <p style="font-size: 13pt; margin-top: -3%; " class="grey-lighter text-start">@lang('Unknown day')</p>
                            <p style="font-size: 10pt; margin-top: -3%;" class="grey-lighter text-start">PLATFORM NR.: NOT FIXED, PLEASE CONTACT OUR HELPDESK</p>
                        @endif

                    </div>
                </div>
                @endif
            </div>
            <div class="col-3">

                       {{-- qr code gen --}}
                @if(isset($ticket))
                    {!! QrCode::size(250)->generate(route('ticket.print', $ticket->pnr_number)); !!}
                @endif
                {{-- <img src="data:image/png;base64, {!! base64_encode(QrCode::size(200)->generate({{route()}})) !!} "> --}}


            </div>
        </div>


        <div class="row passingers p3 mt-3">
            <table class="table table-passingers " style="margin-bottom: 10%; border: transparent;">
                <tr style="border-bottom: 2px solid #cac9c9;padding-bottom: 5px; height: 40px;">
                    <th style="color:#1926cf;">PASSENGERS</th>
                    <th>SEAT</th>
                    <th>TICKET TYPE</th>
                    <th>TARIFF</th>
                    <th>PRICE</th>
                </tr>
                {{-- <tr style="padding-top: 5px;" >
                    <td class="bold grey-darker">Alfreds Futterkiste</td>
                    <td>Included (Seat Nr. not specified)</td>
                    <td>Single</td>
                    <td>Normal - Odrasli </td>
                    <td>180.00 HRK</td>
                </tr> --}}
                @if(isset($ticket))
                    <tr style="padding-top: 5px;" >
                        <td class="bold grey-darker">{{$ticket->user->fullname}}</td>
                        <td>Included (Seat Nr. not specified)</td>
                        <td></td>
                        <td></td>
                        <td>{{number_format($ticket->sub_total)}} RSD</td>
                    </tr>


                    @foreach($ticket->seats['passangers'] as $passanger => $count)
                        @if($count > 0)
                        <tr style="padding-top: 5px;">
                            <td>@lang($passanger)</td>
                            <td>{{$count}}</td>
                            <td></td>
                            <td></td>
                            <td>{{number_format($ticket->seats[$passanger.'_total'])}} RSD</td>
                        </tr>
                        @endif
                    @endforeach

                @endif
            </table>
        </div>


        <div class="row passingers p3 mt-3">
            {{-- qr code gen --}}
            @if(isset($ticket))
                {!! QrCode::size(250)->generate(route('ticket.check', $ticket->pnr_number)); !!}
            @endif
        </div>

    <footer style="position: fixed;
    bottom: 0;" class="row footer text-center grey-lighter">
        @if(isset($payment))
        <p>TRANSACTION ID: {{$payment->oid}}, {{date("Y-m-d h:i")}}</p>
        @else
        <p>Unknown transaction</p>
        @endif
        <p style="border-bottom: 2px solid #cac9c9;padding-bottom: 1%;">Passenger transport in the name of, under the responsibility of and on the account of the operating bus company (business person as defined by Section 4(37) of the Public
            Transport Act), mediated by Globtour ,  Karađorđeva 7 , 11 000 Belgrade , Serbia. Helpdesk:
            info@globtour.rs / +38160555555
        </p>
        <div class="row">
            <p class=" text-center  pt-1 grey-lighter" style="" >Thank you for choosing Globtour and Jelinak d.o.o</p>
        </div>
    </footer>


</section>






{{--END CONTENT--}}




</body>

</html>
