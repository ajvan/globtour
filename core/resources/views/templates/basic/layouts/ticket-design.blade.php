
<!doctype html>
<html lang="en" itemscope itemtype="http://schema.org/WebPage" style=" zoom: 55%;" >

<head>
{{--    <!-- Required meta tags -->--}}
{{--    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>--}}
{{--    <meta charset="utf-8">--}}
    <meta name="viewport" content="width=device-width, initial-scale=1">
{{--    <title> Ticked Design </title>--}}
{{--    <link rel="stylesheet" href="<?php echo e(asset($activeTemplateTrue.'css/bootstrap.min.css')); ?>">--}}

    <style type="text/css" media="all">
        @import url("https://fonts.googleapis.com/css2?family=Georama:wght@100;300;400;500;600;700&family=Lato:wght@400;700&display=swap");

        /*@page {*/
        /*    size: A3 portrait;*/
        /*    margin: 0.5cm;*/
        /*}*/

        *, ::after, ::before {
            box-sizing: border-box;
        }


        body{
            font-family: "Georama", sans-serif;


        }

        body {
            margin: 0;
            font-family: var(--bs-font-sans-serif);
            /*font-size: 1rem;*/
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: transparent;
        }

        .header, .departure-operated, .departure-arrival{
            border-bottom: 2px solid #cac9c9;
            padding-bottom: 0.5%;
        }
        .header h5{
            color:#9e9e9e;
            text-align: right;
            font-weight: bold;
        }
        .header img{
            height: 50px;
        }

        .header{
            display: block;
        }

        .header div{
            display: inline-block;
        }
        .print-warning{
            border: 3px solid #dc3545 !important;
        }

        .grey-darker{
            color:#777777;
        }
        .grey-lighter{
            color:#9e9e9e;
        }

        .bold{
            font-weight: bold;
        }
        .gb--connect-line {
            background-color: #dedede;
            margin: 0;
            width: 0.2rem;
            height: 135px;
        }
        .gb--connect-circle {
            width: 20px;
            height: 20px;
            display: block;
            border-radius: 99px;
            margin-top: 2.5px;
            background: transparent;
            border: 3px solid #777777;
        }
        .gb--connect-circle-green {
            width: 30px;
            padding: 3px 0px 0px 6px;
            height: 30px;
            display: block;
            border-radius: 99px;
            background: #7ed321;

        }
        .gb--route-details .gb--connect-divider_vertical {
            -ms-flex-direction: column;
            flex-direction: column;
            padding: 1rem 0.5rem;
        }
        .gb--connect-divider {
            display: flex;
            align-items: center;
            left: 0;
            top: 0;
            position: relative;
            flex-direction: column;
        }

        .table-passingers tr th{
            color:#777777;
            /*font-size:20px;*/
            padding-bottom: 1%;
        }

        .table-passingers tr td{
            /*font-size:20px;*/
            color:#9e9e9e;
            padding-top: 1.5%;
        }

        /**/

        .row {
            --bs-gutter-x: 1.5rem;
            --bs-gutter-y: 0;
            display: flex;
            flex-wrap: wrap;
            margin-top: calc(var(--bs-gutter-y) * -1);
            margin-right: calc(var(--bs-gutter-x)/ -2);
            margin-left: calc(var(--bs-gutter-x)/ -2);
        }

        .col-4 {
            flex: 0 0 auto;
            width: 33.3333333333%;
        }

        .col-12 {
            flex: 0 0 auto;
            width: 100%;
        }

        .bold {
            font-weight: bold;
        }

        .text-danger {
            color: #dc3545!important;
        }

        .text-center {
            text-align: center!important;
        }
        .pt-2 {
            padding-top: 0.5rem!important;
        }

        .mt-3 {
            margin-top: 1rem!important;
        }

        .grey-darker {
            color: #777777;
        }

        .print-warning {
            border: 3px solid #dc3545 !important;
        }

        .justify-content-between {
            justify-content: space-between!important;
        }
        .flex-row {
            flex-direction: row!important;
        }
        .d-flex {
            display: flex!important;
        }

        .grey-lighter {
            color: #9e9e9e;
        }
        .pt-3 {
            padding-top: 1rem!important;
        }
        .pb-3 {
            padding-top: 1rem!important;
        }
        p {
            margin-top: 0;
            margin-bottom: 1rem;
        }

        img, svg {
            vertical-align: middle;
        }

        .col-9 {
            flex: 0 0 auto;
            width: 75%;
        }

        .col-1 {
            flex: 0 0 auto;
            width: 8.3333333333%;
        }

        .col-2 {
            flex: 0 0 auto;
            width: 16.6666666667%;
        }

        .col-8 {
            flex: 0 0 auto;
            width: 66.6666666667%;
        }
        .col-3 {
            flex: 0 0 auto;
            width: 25%;
        }

        .text-start {
            text-align: left!important;
        }

        .text-end {
            text-align: right!important;
        }

        table {
            caption-side: bottom;
            border-collapse: collapse;
        }

        .table>tbody {
            vertical-align: inherit;
        }

        tbody, td, tfoot, th, thead, tr {
            border-color: inherit;
            border-style: solid;
            border-width: 0;
        }

        .table {
            --bs-table-bg: transparent;
            --bs-table-striped-color: #212529;
            --bs-table-striped-bg: rgba(0, 0, 0, 0.05);
            --bs-table-active-color: #212529;
            --bs-table-active-bg: rgba(0, 0, 0, 0.1);
            --bs-table-hover-color: #212529;
            --bs-table-hover-bg: rgba(0, 0, 0, 0.075);
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            vertical-align: top;
            border-color: #dee2e6;
        }

        .table>:not(caption)>*>* {
            padding: 0.5rem 0.5rem;
            background-color: var(--bs-table-bg);
            border-bottom-width: 1px;
            box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
        }

        th {
            text-align: inherit;
            text-align: -webkit-match-parent;
        }

        .pb-2{
            padding-bottom: 0.5rem!important;

        }


    </style>
    @yield('section-styles')
</head>

<body>
<div class="overlay"></div>
@stack('fbComment')

{{--CONTENT--}}

<section class=" ">
{{--    <div class="container">--}}

        <div class=" header " style="display: block;">
            <div class="col-3" style="display: inline-block;">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAbIAAACpCAYAAABOFbjwAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAgAElEQVR4nO2df7QdVX3oP2Zl5eVl0eSaiZRSjHGwKVLKg0ugiPgLEqTU7qImoBVlIiVR+2yrVq6UUpePZ+ONj1pEpInKnVKlCHlWR9TWXJWngChySxGRqgyRYsqjmXCTl5eVl5UV3h97T+7k3Pmx58eZc849389aZ51z5sye2efMPvPd3+/+/njec889hzA8OL5aBCw1j8UdjxFgCXCMeb8QWJTyep55v9AcdnHG6V4YecFTXfkigiAIhvm97oBQHcdXC4FjzWMZcJx5/QLzfhkzQit+tHXNp0SICYLQBiLI+hDHV8cAxyceJwC/knh/nHle1Ks+WvAPNjsZDXFe5AX7utwfQRDmKCLIWsbx1Ty0IFoOrDDPLzLP8bYsU90gEeR96PjqBGACWG3ePwPcD3wJ2BZ5wd6u91AQhDnB82SNrHkcX81HC6SXmMeJidcusKBnnWuHHZEXvDjrQ8dXi4EHgJUZu+wHvgB8JvKCu5vvniAIcwnRyGpgTIAnAacALwVONo8VaIeIYeWLBZ9fS7YQA20yvQy4zPFViNbcbo284MmG+icIwhxCNDILjDlwJTAKnIEWXCej166E2ZwfecE30z5wfLUM+DnV1ve+DmwB7oq84GCN/gmCMIcQjawDYxY8FTgNLbRGzft+dqzoJ/YD9+R8/naq/5YXmMcux1c+sCXygp9VPJYgCHOEodfIzHrN2cArgHPN64W5jYQ87oq84HezPnR89c/oSUJT3A18Cu0gIlqaIAwhQyfIHF+NAOcBr0ELrlMZ7vWspnl35AWfSPvA8dVJwI+7dN7dgA/cLFqaIAwXQ2FadHw1CvwecCGwChFc3eSrOZ9d2MXzLgXeC7zX8dUkcBNaOzzUxXMKgtAHzEmNzDhnnAO8GbgYHTw8bBwA9prnafOc9voQkAxGjrOFLEebAJeWOGcYecGJWR86vvoSoEocry5Poc2OWyMveLrF8wqC0CJzSpA5vloJXIl23T6ux91pgt3ALvOYNu93A1Hi9d7EYzp+3YQmYiYErwY+CLzSosknIy/4w5xjPUtvgr0PoUMCbpK4NEGYewy8IDM3yDcAf4xe8+p3dgFPAzvN89PAL4BnEp/tAnb1k1nM8dXNwDsKdntj5AVfyGh/FvC9xjtWnseAmwFfsocIwtxgYNfIjJu8B1yDDkDuB/YBoXk8Bfwb8KR5/RSwc4A9696N1s5Oytnn7pzPzmuyMzU4CbgB2OT46ja0lvZQj/skCEINBlIjc3x1EfAx8rNDdIPDaIEUC6vHE6/DyAt2tdwfaxxfLUCHFsQZ8I8xH42Y53noEi4xi5hJpTUPbRI8BR1Xl8ZDkRecnnP+7Zi8in3I/Wgt7Y7ICw70ujOCIJRjoASZcZ2/GXhTl0+1F3gE+AnwQ+BR4GfAk4OkURnhtRa4HL3G1c34uL+KvOB9Of3Y0+XzN8Eu4NNoF35JhyUIA8LAmBZNDNKX0Yl3m2IaLbAeBX5knh+NvGBng+doHWN2fRc6p+Gylk77rZzPzqH/hRjo3+oDwFWOr74I3BB5wbd73CdBEAoYCEHm+OpU4BvUuynvBKbQWdcfAn4w6AIrDRMz9zny17Ka5jD562OvaakfTRE7EL3B8dXDwI3AbZEX7O9ttwRBSKPvTYuOr04B7qWc2/ZTwA/QQmsKvX4z5+OIHF9dBnyG9svE/CDygjOzPnR89Q36x9mjKtNos+NNkRfs6HFfBEFI0NeCzPGVC3wXHaCbxWG0hnUPWuDdMxc1rSIcX21AZ4avygHgYfRv+SNmQgR2oxMBHzLHvyil7V9HXvCejH7NR6+PzZWky4fRRUNvzMrwLwhCu/StadE4dnyN2UIszq5+r3n+fuQF+xhijBfnzRWaHga2oet9fbPIkcWYLdP4Tk6zUeaOEANtdrwYuNjx1aNos+OtYnYUhN7Rl4LMBDnfiXavP4x2j54EtgP391OgcK9xfLUCvSZWNn/kPcAVkRf8xPI8y8nOlpJXtmUQgtSrcjJ6ArHJ8dUtaLNj2OM+CcLQ0ZeCDO1e/xh6tnu3ZGDI5W+ZiQWz5dPAO0tOCM7O2P6zyAueyWn3ihLnGFRGmElYfBfa23Gyx30ShKGhLwVZ5AW3Abf1uh/9jnHusMmBmOSrwMbICw6XbPeyjO152hjMbY0sjdcBrzNmxxuAz4rZURC6i5QzGVAcXy0EPlqy2V60ObGsEINsjSxzfczE/rUVx9ZvnIx2jvmF46txY5oVBKELiCAbXDZQPsP/x6uEIRjPw6yqzvfnNB02bSyNEeAq4AnHV3c6vjqn1x0ShLnGQAgy48EoGIxgeX/JZoep5tkIWoilZebYh17LzGIY1sdsmYdOF3av46sHHV9dZlJ3CYJQk74XZI6vFqFvAMIMrwNOKNnm+zXi61ZlbL+/wEyZZY4cdkaBvwP+zfHVXzi+youTFAShgL509ujAQwfjCjNsrNDmyzXOd0bG9u9nNTBadFvVCe4zj5+i67otQJtdXwqcah7HZLbuHccCHwKucXx1O/AxKSkjCOXpa0Fm4sneB1zd6770C2b2fkGFpnWS32ZpZN/NaXNWjfPZsBvtFfhpG03T8dVL0ALtdLSp9DTKa7XdYgHwNuBtjq/uQZcoCiReUhDs6GtBhs6g4KJvWoLmEqqZhCvN9M06zikZH2dqZHRXkH0CuDbygmnbBpEX/AxdiudIBWvHV0vRZr5RtNY5SrPVFapwrnk86fjqJmBrme8pCMNIvwuyuL6V/JFnWFehzc9qpPE6lfRxsqMgEPq3Kp4vj13Am5sKNo68YDc6Y8yR4zm+WszRwu0seiPclgPjwAcdX92KDrLOc6wRhKGlbwWZyXofuyqLRsaRdacqLu1WaagyyDIr/qCgXdMa2WPAb3c787zJInM3ibI0RnNbhf5OLzPPbcXHLQLeAbzD8dXX0eto/9jSuQVhIOhbQcbRDg27etaL/uICqpkV6+T/y3L0eDCrgQn+bdIT7xHgNZEX9GQcGM3t6+YBHMlxeRZa8zyLdpIjXwBc4PjqMfT6oCQrFgT6VJAZl/u3mbeHJdfiEX67YrsnapyzikbWpNt9SA+FWBZGM9wB3AFHHJNOYUa4nY3O7tGNEJeTmElW/Gl0SZknu3AeQRgIGhFk5k98KjPJaxej/8DzmXF7XoT2zrrdIrvEJcwU0uyrG1iPqVqc8qkqjQocPaZymmYW2SzJbuD8fhNiaZh4uofN49MAjq+OQQu0c9DB4WfTbBjACPCn6GTFX0CbHe9r8PiCMBBUFmQmu8SFwOVok4dNBef9wN9Y7Hdl4rWsj3HElFU1X1+eU0YeJ5Pt6JF3XZrQyA4DbxzkaszGweaIM0lCazsX+B3zXKbyeRZx1pC1jq+m0O77dxTVlxOEuUIlQeb46hJ0IOdJJZv+Y+QFBwqOvYIZJw8QQRZTNst9ktL5FQ2nZmzPNCuam3VWAc4yXBN5wd0NHKdv6NDaPglH1hPfDbwBHWpSlzhryEcT7vtVJzKCMBCUEmSOr05AVxNeXfF82y32eVPHe3G912SVUbGh6o3s9Izt/5LTZiX1nR7uBjbXPMZAYNa23g+83/HVMuAP0I5OK2oe+jjgOuBax1e3oc2OD9c8piD0JdaCzGTt/gfsvdF2otMG/Qs6EPUZ8gNoY97S8b5q/NNco6q57lCBGTCPLI0sL7g6K0u+LfuAyyuWmhlozFrgR4CPOL5aBVyBntjVSZq9AJ3mzTNZQ65HZw0Zut9XmLtYCTLHV+cBXyE9A3qSQ4APbIm8oCjOKO08pzDbuWDoBZnx4swSKkXUcZTIEkp5gixLi7NlTDzwwPx/fuD46j3oDDfr0ZaQOl6QcdaQpx1fXY9O7yUWD2HgKRRkRhOzEWL3AW+NvKBOzNKlKduGXpChXeCr3sAqZbw3ZuSlKR/tjrwgzwuyjkY2hZ0z0NBg1pRvB253fHU8OixlPfUSMh+HLsq6yfHVNuBDkjVEGGRyBZlZiP4SxULsDrQQq+sldXHKNpkx1vMCrLo+liWQitZZ6jh6vFNMXtmY5Mix6fFstEB7E9U9H+eb9m8yQdYfQns7yjUQBorMWb7xPvt7ilPx3A+8pUiIGXf9vM9XkB6z9H8Kzj8M1MlbWNW0WHp9zGgMVVM3bY28wGYNVQAiL7g/8oKNwC8Db0ZnHakjgE5C/9/3OL7aIsVshUEiz1z1pxztBp/GAXQSV5tyE0Wejq/L2C6mxXp5C6tqZP8lY/s/57SpalbcD1xbse1QE3nBgcgLbo+84LXAC9Elj+rk1jwG2AA86/jqAcdXVUoGCUKrpGpJZn3kgxbt/9omYNUkXX05kJfs9PcytufGnc11jEt2nbpZ/1GxXVZGj0dy2lQVZH8lsU71STE9XsHRWXLKsgr4J8dX08At6LU0SRcn9B1ZGtmHKY4FOojOIGDDxeSYPUwqnzoBv3OZusHFpU2Lxgyc5kxwGHg0p2kVj8XdaMcDoUGM6fFKtOnxLSRK1VRgBHgvWkv7nmhpQr8xSyNzfHUycJlF220lZtGXomPQsjgXHe+SxrAvPNcVZFWyeqwkXVsPCzKzVNHIrpNZfvcw1+s24DZjaYm9HqvUWJuHNnPHWtpWdKB11cwxgtAIaRrZWMb2Tj5ncwKzaHweOig6i9fkfDbs+eKyyqjYUsXZI8vRI9OsaLTqsjfHpxF3+9aIvOCpyAv+MvKCX0Ob+j8NVJ1EjABXAf/u+Opex1evMw5igtA6Rw08x1fHAr9v0W4vidpMBVyEnt3nCbI8R5Ai1/+5TuumReA3MrbnrY9lranlcX1R7k2hO0RecF+Dpsdz0B6Pv3B89WHjvSoIrdFpPtqQsi2Nuy09FQF+F53xIzVbgyktXzet0ZzE/DZ1E8lWMftkaWQ/zGlTNoH0bkQb6zkNmh6PMY8/AV7p+Go7WjjeL3FpQrfpNAVcbtnuGzY7GaeBi9BlP7IG87kp/RA0VdNSxRw0pUTKcnLG9jyNLEuLy+ITFfsmdImGTI+L0P/pD6E9n9eKyVHoNkcGmOOrc7GfhdmaIeJ6S3lmxaKs7sMcmFn3u+8o28Dx1ULStcDD5McnZQm/NPZj7/Eq9ICGTI8XAJ8Hfur46k+MhUEQGic5U3qjZZunIy/Ic8FO8lrznCfIioJ9m6yoO2jUyVsJsKVCm5NI15DDAnNyGUF2iySrHQxMwPVtkResQQdcX0P+/zkNFz1x+XeTNaTMWBGEQpI3rLWWbe4ucfw43uTxnH2KBNkvlTjfnMJMGO6v2PyzwF9XaJd1k8lMKmuy868ocY4bynRI6A9STI+3UC7zziL0OvyPHF99y/HVxUWp6wTBhnkAjq9OxT57xHdtdjIZKWKPu9QZnOOrlRSbz4bZtAjarLOjxP67gI2RF7y14iL7SzO255kVyzh63BV5QdkZvdBnGNPjFcAL0GO0bK7HV6NjS59wfPUBc78QhErEGtmFJdrcZ7nfeYnXWSayVRbHqZOeaeAxZXF+A51DL0srmgbuQnubvTDygq01TpkllH6U06aMqej6EvsKfU7C9BjnehwjP/tLJycAm9Cu+39nykYJQilitX6N5f77yS+qmOT8xOusGXhWYtokx1meb84SecF+ZnLojaDXHBajr8fOgvpgZckSZHkaWZYW18nDkRfcXa47wqBgcj1uBjY7vhplpsyMjba1AJ1R6DLHVw8DNwG3iWerYMN84xprW+/q+yXix15tnp/KKfFiE0QrwZUJjJPEVDeObcZCliDLm2XbBkPfVK5HwqASecEUMGUqXL8OHdpzEdmp6JKcinZUut7x1a3AzZEX5IV+CEPOPPRNyNYz0MrxwNi746SzeZ53NjfA40z6I6H7uKQHxO+OvGB3TjubNbJ96MBbYYiIvOBQ5AVfjLzg9cCvAH8I/MCy+THAu4AfOr76juOrNzm+shGEwpAxj+KaY0m+Y7nfqxOv8zJ6LLc8Xp2y7oI9VTwWF2AXf3irmImGm8gLdkde8MnIC85Em6M/Auy0bH4uM2mwNplCvIIAaEH28hL727qCvyLxOmv9poyDgMSdtEPWhCFvfWwldplZbi7fHWGuEnnBY5EXXI12EFmDDhfZb9F0GfABtLfjVxxfXSSZQ4Qy62OPFZiXkiSP+fOMfcrkcrNxChHqk5Vm6sc5bWxyQd4jaxxCGpEXHI68YDLygreis4isB75t2fwi4CvA48aF/9hu9VPob+ZhH8hquz62kKMztqeaFoFfszwvaLOC0H2yJheZpkXsBFmVDCPCkBF5wb7IC/zIC14FvBi4FrssIiuYceH/e5NuTxgi5mEfxHiv5X6ncbTDQJYgK5PVfdQISKG7ZJkW8wRZ0YRkL/CFat0RhpXIC3ZEXvDfTRaRl6ErJRRZhOaj3f2/4/jqh46vNpisM8IcZx7wfct977Hcr9NUmbWYW8a0uIDiVFZCDYzzTZpp5hD5nqdF1/E2EwcnCJWIvOD+yAveifZ6fD3wRYoL7p6CtgT8wvHVdZI5ZG4zD7jOYr+dkRfkzcqTnJl4fTBnXa1snS3bpMZCNbIE0o6C2MEiQfaZiv0RhKOIvOBgiit/0ZLHCPDnwM+NQJNQnjnI/MgLvu746gq0V1lajMZh4H0ljplMO5Va1NGYCcsuzL4B+OOSbQR7ssyKmdqYSfiaF0LxcOQFtjFDA4nruiPoRLi5hGG4uYXuDA1mgvxJ4JOOr16CDri+jOw1/0VogfYHjq82Rl4QtNJRoRXmA0RecIvjq0n0DOc8dFqoA+jAxY9FXmBlfjT26OQMPcusaBs/luQEx1er5vqNsYdkCbK8xfbl5FcUHwZt7BJgvGgn13WnwjCsUtNLKMAkob4WuNY4elyOvi5p9c+OA77k+OpvgHeXyFQk9DFHbkKRFzyJTvhZh9M4OqYoVSOjmiAD2Ih9VgChHFlOG3klePLMw4cYjkwew16doa+IvOAe4B7HV+8GFFqoXcDsCdc7gOWOr16fk0JPGBCaDiQ8teN904LsMlm07RqlTYvkr4/dFXnBrhr9EYTKmKz8d0Re8DvAr6KXJTpzlF4E3Nh654TGaVqQdQbUZt3IXlTx+Aspt14n2FPFtJjnev+3NfoiCI0RecEzkRd8PPKCM9D3qM3MLHtscHy1une9E5qg6eqsnUmA/3fGflU1MoA/cnx1oykZITSAKQ2TZSLL08g6TYuH0GNqN7o+WmVc191CsRPFNLCu7tqT67obsAvaXheG4bY65xJ6i6m6Pub46mp0TtjLgfcDs8bQkgl1J7C2C90I0drhA8C2PeuDvP+YYEHTGllnTsQs1/s6xTIXATfUaC/MJmut65mCGLAs0+JtDSyi2xRdHUGvm9al0FnDYFu3T+hzTGqsb0ZecDk6Ni2Nbggx0P+3tehx9/iSCXXnkglVNhxJSNCYIHN8tZTZLvVZpsW6NcbWOr66pOYxhBmyBFJReqDOP1+s4U/U6w6QMkPOoJZZyHVdF3uHjQfrnEvoT/ogYH8tWqAVhnEI6TSpkaXVFssSZE0k9/yU4yubOlhCMVmzwbwYsmVo7biTx0xRxbo8YLnfiOu6o8W7ZVJGEIr7vNBNtogwq0aTgizNWWBv5wYTRNuE5+Fi4GuOr+qYKQXNiRnb82z3Weucf1ezLzFlhIaNGTKLrO/eSRiGYdrvsQ29VpfHNBI2ItixZcmE6pZJc87SpLPHb6ZsmyXIaDbuZgVamL1GXL1rkaWR/TSnTacg24eu6NtI7FgYhtOu605xdCWFLGyFURq22lyqYDXC7fk1zi8InYyjJ0iCJU1qZGnFL9MEWdO5zk4BviXxZbUobVpktiCbB9wXecGORnqksdXK2jAtyvqY0BaumBjL0aQgm+WKHXnBgZT9upEJ4RTgXjEzlqcgX2KeIOuMBVxEc2bFGNt1skoOH67rlml3R5VzCEJFmvDGHRoaMS2aUuOdN8O2PYFWooXZa0tk6hf0dUub0ByMvCArM0vcLslhmr/ZW6+Tua67ukI8ma0mNxWGYdE6mDCcTO1ZH5xRtJNxrx9FCyibCVQdK8PQ0ZRGdjyzhWKaNtZtlgPfk0j9UmSZFbMKosYkBdkB4Ks5JXsqYYRHN82Ltmtr4q0oZGE15vesD8I964Nte9YHa7AcT0sm5D5mS1POHitStvVCkMGMN+P7Ii/4eI/6MEisyNi+o6Bd0ow7DXyuic6ksB27GWwVhw9bb8dME6eJQ7uTfEG6LQzDdWU6Zo49gs7ifmLi+C7la/mBziRxZRiGlUMjMvqziuzlgqS35hQ6AfUdVbVbk4FljPw13fEwDLem9HmN6WdyLE0Dm3pQYmcLdmO6cBlmyYTagv5+WftOAevKZg9ZMqGuAq4uOO6Ve9aXC7VZMqEaH9N71gdT3RRkhzP2bXTWnsF84AbHVy8Hroi8YF8L5xxUsvJeZmpkxpR8XMfmLzbWo6Ox/aOUmr2aG1wtj0XDWovjrC1j+jTCcQyLOmclGEX/RqUFWY3+JAVH/LzFdd2taIFTNjXTOvJveHE/twK4rlt0Mx4xn7cqyPasD7YtmVA2u+be3JdMqFGKr8koeoyW/Y5F2W5G0b+d1QTNmFa7NqabMi2uSNmW5rEI8ExD57ThEuCfHV+d1eI5B40sR48nctocy9Fm6ccyHHtqU2LdyzXCyRZbbazV9TGjdTxOs3/4ypj+PEiz/dkAPGiO3TQuHMnVOU6xVjPIZXiW9vj8Vr+d8cDs6phuSpC92HZHc8NrU5i9BPiu46sPOb5Kq4A97KzI2J63RtbpHdpESqo8bIVZmcDoJrSxRnFddy12yYtbIZFMuRs3+xG0dtb49zX97ouJwLBjgru7Pqa7qZHl0XaWg3nAX6C1s3NaPne/k6WR5QmyZK7MfcBnm+tOKtst9yvj8HFmw+euhdEmP9XGuWxoUahu6IJmZpsEWugiZj2slTHdlCBLi99amLP/dxs6b1lORrvof8bxVRP5Hgcas9aVFXu3I6dpUpA9EnlB1npoU9hqRWWy01utqdUtEVOCDfSJmasHQnW8pFm4iL74HYX2xnRTgixNKOQJsm45BtjyduCnjq/+dMjNjWlhE6AddZ7KaZd0ELmn0R6lYDztbNaprEyLJTLet+l2f2mL5yqibaE6whCZAo2mMgy0NqZrCzIjCNIuTKYgi7zgEeD+uueuyWLgo8Djjq/ebjJcDBsrMrbvLKgnlkwH1lbqJhuhMmKEVBG2Jsg2zYr9FADbi6wSvcpk0YuilrYetk1UkegJRli3NqabuHln5Tgsyql4LS3dKAo4AfgMcI3jq+vQRSEP9rhPbbEiY3tRMHTStNjWeucD2BU6XI1xv87Bdn2srRtJGSeVSXTcWtkb8G6bGDIzEbCN6ZlGZ3N5POPzE8mPceo89WidOLcU4oD6vFRnrQoyc4O/2nL3Qa6Y0MqYjuPYmhBknfFEMYscX83PmtlHXjDp+OqzwGUN9KEJXLT33XWOrz4K3DIE8We/nrG9SJDFpuS9kRcUFd9sClszn01gtNVMscX1MVvCMAy7XaXadhYdAmuKYsFc1x0DvmF53FU0N3nYhg4A73boxNISGTjiFFU2E4Wte9YHw5AWLTTZTmrRhCDLc5pYTr6UfSc64e9pDfSjKU4AbkALtE8DNzac0b2fyPrddxS0iycvrZk+wjCccl13muLZvc0N0+bG029CDNrRHmy1MauAZlOOZxN6xl1EU2tHU1UyqVRklOYtS9MMj+dlI2O6CWePIkGWidF41tCftuDFwHuBJxxffc3x1cVzaR3NeCyenfHxzwuax4LskeZ6ZIWNcMkVUiWqSfeD2buTVQ1799XBWtCHYdh2ba1NLZ+vacbKppQaYFY14fzShCDLMi2CDkbOxRTEfAVwawN96RYXAv8A/Jvjq3HHV2m11waNs8le38z0WHR8tZQZTf5fmu5UAVbCpaA8i60ZqE2NzDZt2wjwjX4QZhXSS7VJP2rTNoTAmj3rg6I13kGg1JiuK8yaEGR51XF/y+YAkRfsj7zgcuB3gLbWXKpwHHAV8CPHVw84vnqv46tcrbOPWZ/z2c6cz5IaeD9qZJBvXrRZQ5tu2OkglxLhBaC/2xNdSu80JxjAkjvT6DyEZ+xZHwyqED4K44RRakzXKSbahCDL804slUUj8oKvAi8F3go8VKdTLbAKuB74ueOr7w2SUHN8tYJ8J5u8GLKkFvdoIx2yxGgBNppAnrCy8abqxc2kTC23OL3T9pLFQYX+JPZkHDfJdecKpcf0kgm1vUr5miYE2eKcz052/HIXJvKCQ5EXfDbygtOBV6FdqXfV6WALnMWMUHvQ8dUHHF+t7HWn0jBrY58iO87vYOQFebkwY43sycgLshJDd5PK62Ql4rVsK1M3yTj2M9iY1cB213W39IO5UahFHBT+eB3NpM+oPKaXTKgtZcyN3RZkAG+qeuDIC74decFG4JeB89GlCKbILhHTD4yiF5v/1fHV446vbnR8daHjq7xMJ21yE/nrRHnaGMxoZA83053S2ARgZ2XCt41taV0jM9rmWMXmG9Dmxqsa7JLQO7bMBWFmHFZqjWlTF62QJrzwigKf3+346q/qlPkwufy+aR44vloMnIu+MZ2BFh5ZOQN7iQv8V/M44PjqbuBr6GrKra4FGieNzwAXF+yatz4GM6Uj2l4fiymTCb9zXxttrNX1sSRhGG41iUmqJOsdQecsvBQY63YMnOu6D9JObcFBwPa3Xop9nN6WJRNqctC9F/esD7aa2muVx/SSCXUp2pMz83duQ5Adh3Zj/8sGzgWAMWl91TwAcHy1DB2TthK9znaSeX0C0O18ivvRKvTuxCN+HyVex8/kBYs3ieOrRejZzTVkeykmKdLIXmCef1inX1UJwzB0XTekON5plNk3GJuMHmXs+o1jhNlutPm3irlwFG1urFq8ssx5BJgsE9Br1sDGsctSs5HqGk3fYIRZ7TG9ZEJtBcbThHsbggzggzb0CKwAABaHSURBVI6v/jHyypXFLoNx47/bPI7CZLo/wTyWon/M56PNorF5dQGwCC2UDgIHzOP/oUuV7EULovh1/H53v6W0Mutg5wBvBn6fcoPHNqtHrzQy0AKqyPSSJrRsbr5t5Y7MJAzDba7rTqL/+DY3vDQ2AJe4rrspDMNWKyAL2Zib8LolE2o7xaEga5kDggyOVMVuZEwvmVCb9qwPjhrTTQiyojUy0ELiy46vzo+84LEGzlkK47zwDP0ZeF0bE6h9Kjo27FXoP0jV6rG/KPh8GXAIaP06JthOsSA76iZRIodgX7g/GxfydcYrcQz7+LckSXPj+QPolj6XsRFkc8mDEZNya53xSqw1po258fw4jVebmSqOB77n+OqdkRfc1uJ55xRGaJ2ETi91OnotaBVam2yCItPiOmBej7VQ60z4CdOajTYW9lugr1nrmjRxY2NUu7nFsWfn92r9T5jF0F4Hs9Y1aRxaao3pJRPq/D3rg6m2Uy4tBj7n+OoK4MORF3yz5fMPFCbe62Tz+A201nUK3V3zyxVk/ZBI2eTvm6JYOI0yE3c2MNpYGmEYbgW2Gs/Eqym/1hBnBXmxaGaDw5IJtXquBEl3YjKYbDWeiZXH9JIJ9eJe5Q48DzjP8dWjwBeALwNTbTg/9BvGm3AlOp2XC/waWuM6mea0LFt20N+ZVZJMUizIzkRnQQe76tE9Xx8rIgzDzcaR42p0lpkyxJWfm0ioaxucXoY5ecMW8tmzPthsHDkqj+leJ8GNtY0/R7unP4R2IvhX9A11J1pDeNq44A8Ujq9G0M4RxzPjbPIidDLlE9D1wGzWGLvN/eiA7i8M0O9sE7ScFHSDmvF+FkajGnNddwv2HnAxaztMrlX7YJPqSxCsMGtdY0smVKUx3WtBlmQh2lkhNSO746uka/velMce83zAPB9EeyDG3of70E4KeykOqF7ITOaLkY5tizseS8zzUrQjxLHmsYx21yDLshe4HfhU5AWDWMDPOsOHZcb7vlsfK8L0N3YI2YL9WsNadHIBQegrEl6dpcZ0EzfayoHOJVlKdU88QXMQ+Drw98AXIy/Y3+P+VMZ2ncwIMZuMHm2XGmmMMAwnXdc9A/sClmsQQTYotB503g/5HvesDyaXTCjrMd1Eiqq3UFyIUegd+4AAnYj5BZEX/G7kBbcNshBL8HmLfVajs78U0Yv8io1hzI2DXodL6MBkka+LU3L/vkhEbcyNVmO6tiAzZqnfRC/U5SWbFdrjYfSM+zWAE3nB75lEzL1I8ttNbP7kJ9K/Ge8bpUQBy9ozbtd1qwa1ChrrbCA52C4JlL1WfROEvWd9YDWmG1nDMS7ZH3F89T/QRSjfDFxEc6XLhWwOokve3AN8C7gn8oKhcK82JrWi3WySr07NBZf0EsIlby3Qdp3watd1J+fC79Y2Jqu7zbjM/W33rA+mTR7DItwlE+qqzmwYGf0ap48CsZdMKKsx3agzgnGfvwu4K5Em6Vy0ZrAKWeOqy0F0DbCH0Kaw7wMPDWPYQoJJ6ptCBlobM5n+L0HfhOpia8oaBR40npNNmL8GztnGsHTJhBqJM0zkYRwYRrGPmbIZlzbxlDCTDSPLHO+ghWtfKB9GqFqP6a551Rk37nvM4yMApvDkacxkpTgN7YrexFrdXOIA8BPzeAz4Edpc+JMhF1pp2KT6KaLn62Mmm3xbiXgzBUaJpMyYfZoQnvpgrrum21n7u8Ao8KylZlQWm3FpE08ZM1pi39osmVCtjenagszx1fLIC4oSzQJg9nsS7XwQt1+AjqeKA4JPNM8uOnO+Tcb2QWM/Oj5uJ/r32AE8bp532P6eTWO06JHICwapPEcT2kA/3DzbzCZfFPgdx/K0TVrFgmHGZn3o85QPIm6L1sZ0LUFmSoQ87vjqFuBDkRcU1bKahcnZF2sfaeeYz0xQcZzFfhm62OaIeSxOed2Glhdnyd+L9g7cja5mvcu8/o+ObbuAnf3odOH46gTgS2izx9d73B1rzDrZNNVNIsO2zjNNcamaOMtCX5iZhpTNNrXI9qwPpkxW+b7wNOwR03U1ssVoYbgB+H3HV1dHXvCJ+v2awZjSdlJc8PEojJCNS7MsMP2MS84ky7fYMM1MgPU+4GA/CqOqOL56O3Az+ncqPRnpAyapXhpie5MdGQA2FQluE6M3RrViiEJ9QsqFUowxAOnVusimuoIsOWM7BrjR8dW8yAs+XvO4tTFxUnHBSyEFU2l7O3BWYvMghlA8QHVBNkxZyKdsa5OZAp9nYOddJzTHNLDOxnkkxmhlG2lu4tHksbrN1J71wea65rc008MVNY8ptIDjq3ehTZ9ndXy0qwfdqUvldZUBdC6oyiRwfpkGYRjOiQrFA0SIrrFVenJlMslvbKAPW82xBoEjY7quIEtLeCt29T7G8dWpjq9+CNzE7HIwzwxQ0uAjmBpbVTTvgU1LVYIQ2BiG4Zoqa4FGgzuDwXHCGEQX/mn0hOGMOpk8jACqc63G9qwPmhCG3SYENu5ZH6xpqrBmmtDqeb0qYTaOr45F293fnrPbIJoVY8bQnna2E6mmUjptQ8+E89zVQ4qzMDS1YB+XV9mONiXWFkBmorDGVNleiy6P49KsV9o0xWZem1CLJk1iW+mOaXUaPR6mgAdss1fYYAThmiUTahT9W61hxgs8qx/bgW0dziVF332a4t+6K2M6rT7b85577rnKR3d89Qfo+kZJdkZe8KuVDyo0iuOrhcAfAR+kuL7ZZOQFTaTOEQRBaI26psVjUrYd7/jqHTWPKzSA46u1wI/RmopNkc5B1sgEQRhS6gqyhRnbb3J8dWHNYwsVcXx1tuOre4E70cHmtgyi670gCENOXUH2n3OO+z8dX7WZrWDocXx1suOrLwHfRee5LMt/NNwlQRCErlNXkOWZqxYBX3N8taLmOYQCHF8td3z1t+icjHWSvolGJgjCwFFXkHW6b3dyLPANx1fH1zyPkILjq2WOr25A52l8WwOHlDUyQRAGjm44e3TiAv/k+GouJv/tCY6vjnF89SHg52iPxKaqGDzd0HEEQRBao64gs72BngJ8S4RZPYwA+wBagP0Fdp6IZRCNTBCEgaPNOmCnAPeammRCCToE2Ca6U6D0MCLIBEEYQNouaLkSeMDxVRWPuqHD8dWI46s/o7sCLGbXIKanEgRB6FqF6ByOBf6X46trgc1y85yNSSf1PuAdpOez7AbisSgIwkDStkYWMx+tYXzP8dWqHvWh73B8tcLx1Y1oDewq2hNiII4egiAMKL3QyJKsQpsaA2A88oL7etyfnmBMre8DLqZ3kwsRZIIgDCS9FmQxClCOr36Czia+Hbg/8oIDve1W9zBFLd+EzpzeDxlQHu91BwRBEKpQV5A1LWhWAn9mHocdX8Xp+58BdqNLBzwL7DWvk897gel+Fn5GeF0IvB4tvJt2n6/Dz3rdAUEQhCr0myBLMg94iXlY4/jqEBlCLvH+WXTf95v3aa8Pm/cAhyMv2IsFpmzKMWgPw+XopL2noysxn0b/aMGd/KTXHRAEQahC3ZvqoUZ60SzzgWXm0RiOXyeF4UDQVxqZ67rjaIeXLLYBV8ZVj13XHQUeBLaFYbgu45gPos24z09WS845VwhsMVWSs/p5J7rYZBHr0FluxoGx5DFd110LXE26iXnS7D9l9h1BT8SmwjA8I6U/Lvp3ADgjDMPQ4rcEXVn4wYJ9yDqvOXepa5Zot4XsIo5jab9/mTaJfq0Lw3BWEcvEsbaGYbjRbHsWIAzD55v38e8ehmF4YtpJzXW8E9gchuFYx/as67vV9LewerftOHVd9yr0OMu7VnFfj3wfs20j6cUwt5l+hon/WhFTwPno3y2PzWEYjiV+Y/L6bvqa7MO2uoJsT832Qn+ww1bj7AGTzK4cvBotPFz0DbgyCUE0jb6pxIygb27jruuSI8y2o28kyb6Nov/4ye1TpFTpdV13NfqGQkqbuMrvN1zXPSMMw7CzfQp3mr5vTNm/8/hJQiD5HePvH5p2MZFFH6yvmeu6cdXnzvPAzO/vdAiG0m2ycF13g2kzFQux4ibuuM2xzc5513etOfcqCsZxmXEahuFm13U3AqOu647Gk6AOLjXPW8zxRxP97Lx+8bUbBU6k2lhJu1YxD6RsG3Vdd3VOhfOjrlVdQVY4ixAGgnt63YEctmTMorcDqwsGey5Ge1mL/pOdkaIp3IkWVBs5+o97hDAMt6a0GQU+39lvfbpZxDfELM0jnoWvzepDYt8t5txbO/tlmNWnjL7EN7YNwLTtTTuB1TUzN/nVZGg5rutuAp4w/Rgz20q3ycJc/3H0fSxVi89gg+u6WywnFnnXdyy2EuSN44rjdAv6u11Nx3cz13Yt+jeM948FQ9Y4jK/dBjO2rMaK0bJI+yyHabRwXIcWqmlcktivtqu3CLK5wZd73YEKbDfPdTw+Y8kylWbaMTeWafS9ZKTz84aIs7UUzVbPzDtIBc2iF3Res/g3Tb1ZmWsSAiPmZlm1TRax9jpmKZRiRtBCwoai6xt/j7zxVXqcGmEUAmtTfoerzfOWlHNkCY742qWaVRsmRGuEl6T974zpdAS4I95WV5BJbr7BZxq4q9ed6BE2wmkT2obf7Unb7qoNE5oFwJXNdKdVqvy2ta5HQnvdlqG9ZjFlHmuNdmhL5etL9XEaC6pYcB2l3XVoXkXp7ybR2l6aGbAbbGHGZNnJpejrf0QQ1zUtShDt4POxyAv297oTNahzQ4vbZt4o8hw92iAMw22u646RPaOHozWLtPWQfqOnlpyE9hpSTfBfiXY0GKfmGq0xtxWZ3CqN08Ra2WrXdUeMkIu19S0du8eCNlWgmXHV2tgKw3CrMatfSsKkbrTLUY5eJ6ytkT1Zs73QW6aAj/S6E2Uxg3kj+g9+R8HumRiTTIj+o99pYYrqCWYBP9X0Zf7so8Bkr4VuHk1dswb6kTQLbqyiaZub+lb02laRN2htao7TWLO52nz3DczWxmDGdLilje9kSfwbJ7/vUU4qMbU0ssgLdju+2gmkVYC+AngxcCq6hEvqSrfQM74NvDHygoO97kgBd2Y4SUyi3anrzu7XoP/Ea9HmIpjxsHqgwDmipxjTVnzT2Z63ryHrt0x1S69Bt69ZHWLtNQR+UOM4Y2iHg0xHoCyMRti51lTkPFJpnCa0sg3o7z2CNkOm7eegx9O4mSBhjh9WcPjpZNR13edStue52X/e9GcjM5pkvA48lRRwTQTnTgJvS9m+K/KCW+I3JlB4pXmcBPw6MwHPUnCzPR4BrgduHZDKA2ku47Fb+jj6D14Zc/M4MXFziY99FYDrulPom28ZZ4C22MLMbzPuuu5kgWkxy/2+aZNRV69ZDeIYqSnTn09RzlvxCGEYThsPyfEy7viGNcyOPXyA7NCIuuM09mDM0sbic4yZ7xSvqcXesrGDRapHoyVZ7veZ4RxGWE1iJgzmu4+gBdxRNCHIfpyx/XIgiN+Y1FEPm8dROL5ahNbYXHQmjBeis2IsB05Aa3y9SqY7yOxDBzo/AnwX+GbkBY/1tkulSXUZj93SXde9qgmTWoob/Sj6zx/HAdVaC+kC8Ww0Fgrb0TfmvH4Wud83RSvXrAKr0SbYNcadfG3Cnbw0SW3HOI/YtjsiPHOCnLPaVhmnW9HCaYTZa2Odx58mETJgzhEHWI+7rhtWHENVwjhAf6fVpg+XmuPMGjtNCLL7M7Zf7PhqZeQFhamPjLPBI+YxC8dX89B1zI4zj+PN868wk8VjmdlnGbCg5HcYNA4Cu9A1xJ5JPP8C7YCzAx3kXMdTqt+Jvae64g5sNJs1rus+jjaLxIvlrWNudmnmo41x7JHrulvRN9ReCQkbunrNLJgKwzAW/BvRDhtjruveUePajqFvtuOkaArdxmacGu1xEq1dlY65NAIb9Hc8k3zHo0YxTh9j6Ovl0uHkEdOEIPs22uOl09tlHvAZx1evqmvCMu2fxtJL0mh4I6ZPIx2PY4BfMs+LzfNC83q+eb8AndB3oXnEVKkPtg+dtxF0DseDied96DRfe83rfYnXz5rnafTvu8s87x5wL8OmqVw126wxbSeRmiiF+Kbg0qLXVoyr0wZdhb55JG8gU8nZeRiGG833sTEx9ppuVjrP48jaUBiGYWwapJ6JcVtCSHTF/NzGOHVnsqWcmGGejLf1wtdhGzNaa6pGWVuQRV5w2PGVD7w35eNz0esx76l7npJ92o8WFlL1ePjIi7nJ+szmxtptbWxpA+fYiJ2Jsd+oEmxeO0DdaBprqGliRGtlD5Kd+xGyr2+Z79HGOC3qTy+sPPEEJMqaoDW17nQ92QmE/8Tx1bgxDwpCU8SZLmJzVfwHXpWRDWAUPZsME6aXeJY5mtEmXred7qKzR3xjyEo83Pk9MzFmxtbcwiuQdc1SA4vNNYl//6kabfKIQwLG0saADeY8m0kXAkXXN/4eeQKojXEat8sK8o5Nsq3XLQzDcDoMw1xHk0ZKikResNPx1WZ0HbE0rgJOc3y1MfKCHU2cUxgaNrqu25meKfbYOpJA1ZiKtqJnxU+Y10lmmSZMm23om0xem26uOcUL9ePme6YlDZ7Gcl2iwMR4acpvGber616dxPaaTRqz3GqzxpOWAHiExO9fpU0eTZkY0VpDnBQ5Sd71jfefDHPyhbY0TuMKAONGS02Om1nXrgIjCZf+TmqHuTRZG+vD6IrHWTbUC4AfO776BHBD5AVPNXhuYe4SJ4ntJC5vcmQma27ij6NvHp0ayTSwqXNWF4bhuoTnmFWbJjE35nVor7K0WXv8PcvMtLNMjJnlZlzX3dSgM0uZa7bGnSmjkqZFzpqJV2mTRxMmRuNQMU7HGo7F9c1b90oep6vj1Li6r2MmRKHz+s26diVxyfbO3ExNB5LnPfdcWoxaNRxfjaLdvIu8Bg8B3wS+AjzF0YUwp9GVnvux1tlQ4fhqFTA1IPFmgiAMKY0KMgDHVx4w0cCh4orN04lH8v2zKduOvI+8YF8DfRgaHF+NoAPVVwEvB14J+JEXXNPTjgmCIBTQuCADcHz1AVLSoPSA2JU99mKcNs/7Ot7/X2Zc3+PPk+9joUrkBQNVusbx1QK0t9NSZuLsjgVehA42X47OrnJcR9NHgdMHIIWVIAhDTlcEGYDjqz8HruvKwfuD/WgT6QHziOPDDmOEHjOeSHG8WJK9zMSX5XGAo2PZ4GjvqBG092kyDm4xMzFyVYLDDwFnRl7wUIW2giAIrdI1QQbg+OpdwI1IeqlB479FXvDBXndCEATBhq4KmMgLPgm8FqkkPUg8xNzWpAVBmGN0XVOKvGASOB24r9vnEmpzEHiLeIwKgjBItGLyM0HQrwCuQd8shf7kPZEXPNrrTgiCIJShq2tkaTi+eglwEzpAWugfbo+84M297oQgCEJZWhdkMY6vLkBnYDitJx0QkjwMvEyy6guCMIj0TJDFOL66EJ2+5ZU97cjw8iTwckkZJgjCoNJzQRbj+OoUdJ6vS9ABu0L3CYHXRl7ws153RBAEoSp9I8hiTLmXc4HfRmtpp6GLXArN8nW0h+KuXndEEAShDn0nyDoxgu0EZtIpLQd+mZnsFZ1ZLBaZxwKzTYKxj+YhYDzygtt73RFBEIQm6HtB1gQmIW6cxqlT2MWpnTo/+085n8Vpn45hphROcvtCZtJKLWB2iqkyxKmwYuJUWFnP08zkityDLuy3G3gG+EnkBVI1WxCEOcX/B9Q7EsDh9sV4AAAAAElFTkSuQmCC
"  alt="<?php echo app('translator')->get('Logo'); ?>">
            </div>
            <div class="col-3" style="display: inline-block; padding-top: 2%;">
                <h5 style="">TICKET NO. JEL-CRB-5020032</h5>
            </div>
            <div class="col-3" style="display: inline-block;float: right;">
               <h5>SINGLE BUS TICKET</h5>
            </div>
        </div>

        <div class="row print-warning p3 mt-3">
            <div class="col-12">
                <h3 class="bold text-center text-danger pt-2 pb-2">THIS TICKET HAS TO BE PRINTED!</h3>
            </div>
        </div>

        <div class=" departure-operated p3 mt-3" >
            <div class="col-3" style="display: inline-block; margin-top: 2%; margin-bottom: 2%; ">
                <p style="" class="grey-darker bold">Departure operated by:</p>
            </div>
            <div class="" style="float:right; display: inline-block; margin-top: 2%; margin-bottom: 2%;">
                <p style="" class="grey-lighter">JELINAK D.O.O</p>

            </div>

        </div>

        <div class="row departure-arrival p3 mt-3 " style="height: 300px;">

            <div class="col-9 d-flex pt-4" >

                <div class="col-2" style="float: left;">
                    <p style="margin-top: 5%;" class="grey-lighter text-end">DEPARTURE:</p>
                    <p style="padding-top: 70%;" class="grey-lighter text-end">ARRIVAL:</p>

                </div>
{{--                <div class="col-1 gb--connect-divider gb--connect-divider_vertical">--}}
{{--                    <span class="gb--connect-circle-green "> <i class="fa fa-bus text-white"></i> </span>--}}
{{--                    <span class="gb--connect-line"></span>--}}
{{--                    <span class="gb--connect-circle gb--connect-circle--orange"></span>--}}
{{--                </div>--}}
                <div class="col-1" style="float: left;">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEMAAAD6CAIAAAB1bW2ZAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAFdklEQVR4nO2dMWgbVxjHL3KMDxqJE4HKdxhSyYssLUYamuDJkBp78FCXE80iaJaO8uLNzZBm6+KMWVrwVE40zeagFjw2HWS8yPJ0akHc2YWgQ07hgrDd4eAolmPffe/J/aN+v+lJvO/77ud77+m9t/jW+fm5MhYk/usHkAab4MEmeLAJHmyCB5vgwSZ4jI/J7RHl9c/6f7z7vd1veIOuf9o/8tuKokyrc+pESpucmUstfXLnUzWRkljxlvTzyZF/sPvX88P+L9f2zKc+W/y4Nq0WpNSVaeINurvHz/e9n2JFzWtfLGZq2uSMYHVpJu1+41V3wz87IcSqieSX915kP7ov8gByTN68/WHH/VYwyYr+zf27X5HDJZj89vb71+4zwSQBy/rmg7uPabGiq3C735CloSjKa/dZ5+83tFghE2/QfdXdEMkwzI9/fu0NuoRAIZPd4y3aFL8C/+xk93iLEEg3OfIP9r2X5PAr2PdeEl4L3YT2l4vIjvM0bgjRxD/rH578SouNAiE50aTzjrjCRKfdb8TqTzQ5jFnmBkoQTXqkhXKkJYgm708lL77iJYgmwXljpMQtMT5nRqJJRs3LfQ7xEkQTdULmwVVKCaJJWviIJ70E0SSfWqIFjq4E0SR7R+ikGoW5mzFRE6l88iEtNgqE5PRVeDGzTo69lhXjSdwQusm0WpjX1sjhVzCvrREujYR+GRcz61OJpEiGYaYSSdrbFjLRJmc+n/lOJMMwj+69oN3iie5W5lJLy/qmYJKQZX2TfH8nYd/14O5jKTIil12K3NvUn7sb70lXLVOJ5COQ29QAb9DdPd6Ke+Eyr60tZtaBbrhDvEF3x3ka5Uohn3y4YjwRdwiQbxLS7jcO+43eoOuf9o/9Q0VRMmpenUilJ2fyqaW4m5FrGaFJSKfTCdvZbHZEVf73Z0ZA2AQPNsGDTfBgEzzYBA82wYNN8GATPNgEDzbBg03wYBM82AQPNsGDTfBgEzzYBA82wYNN8GATPNgEDzbBg03wYBM82AQPNsGDTfBgEzzYBA82wYNN8GATPNgEDzbBg03wYBM82AQPNsGDTfBgEzzYBA82wYNN8GATPNgEDzbBg03wYBM82AQPNsGDTfBgEzzYBA82wYNN8GATPNgEDzbBg03wYBM82AQPNsGDTfBgEzzYBA82wYNN8GATPNgEDzbBg03wYBM82AQPNsFjfExu4v/93gzj807Gx+T2SLO3Wi3btl3XtW07+CaXy+m6Pjs7WygU5NYa1Tyxbbter/d6vQ91SKfTpmnmcjlZFUdiYlnW3t5elJ7lctk0TSlF5Ztsb28fHBwEbVVVy+WyYRi6rhuG4TiO67qO4zSbTd/3gz6FQqFarYrXlWzy77eh63q1Wk2n08PdHMep1+uu6wYfpbwZmWuXbduhRqlUqtVql2ooimIYRq1WK5VKwcdmsxkuCWRkmliWFTR0Xa9UKtf2r1Qquq4H7Xq9Llhdmkmr1fI8T1EUVVWjj3vTNFVVVRSl1+u1Wi2RB5BmEg6Pcrn8oUE1jGEY5XL5QgYa0kwcxwkahmHECgz7hxloSDPpdDpBIxz6EQn7h0sZDfn7LvI7CX9haMg3iTtIwv7B1CcjzSSbzQaNuIMk7B93WF5Amgl54pKXigtIMwl3tc1mM7pMsAe7kIGGNJNisahpmqIovu9H/8Gu1+vBRNc0rVgsijyAzBkf7lBc140iY1lWOEmi7G6uZrR7YdM0Lx39F/bCCwsLq6urgqX5fHIl0c+MpVJJfFwFjPAcb1lWsDu+FE3TKpUK+jk+JLhbCQaV7/uqqgbDLJfLCa5Uw/AdJB5sggeb4MEmeLAJHmyCB5vgwSZ4sAkebIIHm+AxPib/AFnZ9hXD+AegAAAAAElFTkSuQmCC
" style="height: 175px;"/>
                </div>

                <div class="col-8" style="float: left;">
                    <div class="departure-text">
                        <p style="font-size: 15pt;" class="bold grey-darker text-start">25. Feb. 2022, 12:00 | Zagreb</p>
                        <p style="font-size: 13pt; margin-top: -3%; " class="grey-lighter text-start">Friday</p>
                        <p style="font-size: 10pt; margin-top: -3%;" class="grey-lighter text-start">PLATFORM NR.: NOT FIXED, PLEASE CONTACT OUR
                            HELPDESK</p>

                    </div>
                    <div class="arrival-text">
                        <p style="font-size: 15pt; margin-top: 8%;" class="bold grey-darker text-start">25. Feb. 2022, 16:10 | Zepce</p>
                        <p style="font-size: 13pt; margin-top: -3%; " class="grey-lighter text-start">Friday</p>
                    </div>
                </div>
            </div>
            <div class="col-3">
{{--                <img style="height: 90%;" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=http%3A%2F%2Fwww.google.com%2F&choe=UTF-8" title="www.google.com" />--}}
                <img src="data:image/png;base64, {!! base64_encode(QrCode::size(200)->generate('http://google.com')) !!} ">


            </div>
        </div>


        <div class="row passingers p3 mt-3">
            <table class="table table-passingers " style="margin-bottom: 10%; border: transparent;">
                <tr style="border-bottom: 2px solid #cac9c9;padding-bottom: 5px; height: 40px;">
                    <th style="color:#1926cf;">PASSENGERS</th>
                    <th>SEAT</th>
                    <th>TICKET TYPE</th>
                    <th>TARIFF</th>
                    <th>PRICE</th>
                </tr>
                <tr style="padding-top: 5px;" >
                    <td class="bold grey-darker">Alfreds Futterkiste</td>
                    <td>Included (Seat Nr. not specified)</td>
                    <td>Single</td>
                    <td>Normal - Odrasli </td>
                    <td>180.00 HRK</td>
                </tr>

            </table>
        </div>


    <footer style="position: fixed;
    bottom: 0;" class="row footer text-center grey-lighter">
        <p>TRANSACTION ID: 000837, 2022-02-24 20:46:08</p>
        <p style="border-bottom: 2px solid #cac9c9;padding-bottom: 1%;">Passenger transport in the name of, under the responsibility of and on the account of the operating bus company (business person as defined by Section 4(37) of the Public
            Transport Act), mediated by Globtour ,  Karađorđeva 7 , 11 000 Belgrade , Serbia. Helpdesk:
            info@globtour.rs / +38160555555
        </p>
        <div class="row">
            <p class=" text-center  pt-1 grey-lighter" style="" >Thank you for choosing Globtour and Jelinak d.o.o</p>
        </div>
    </footer>


</section>






{{--END CONTENT--}}




</body>

</html>
