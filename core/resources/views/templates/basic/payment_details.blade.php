@extends($activeTemplate.'layouts.frontend')
@section('content')

<section class="orders my-5">
    <div class="container">

        <div class="row">
            <div class="col-12 col-md-7 col-lg-8">


                <form action="{{ route('ticket.confirment', $ticket->pnr_number) }}" method="POST" enctype="multipart/form-data">

                @csrf

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title text--base">@lang('Information about the holder of the reservation')</h5>

                            <div class="row">

                                <div class="col-md-6"><label for="first_name" class="form-label">@lang('First Name')</label>
                                    <div class="input-group mb-3">
                                        <input type="text" name="first_name" class="form-control" placeholder="@lang('Enter First Name')" aria-label="first_name" aria-describedby="first_name" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="last_name" class="form-label">@lang('Last Name')</label>
                                    <div class="input-group mb-3">
                                        <input type="text" name="last_name" class="form-control" placeholder="@lang('Enter Last Name')" aria-label="last_name" aria-describedby="last_name" required>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>


                    <div class="card my-4">
                        <div class="card-body">
                            <h5 class="card-title text--base">@lang('Buyer Info')</h5>

                            <div class="row">

                                <div class="col-md-6"><label for="email" class="form-label">@lang('Email')</label>
                                    <div class="inpu|t-group mb-3">
                                        <input type="email" name="email" @if(Auth::check()) value="{{Auth::user()->email}}"@endif class="form-control" placeholder="@lang('Enter Email Address')" aria-label="email" aria-describedby="email" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="tel" class="form-label">@lang('Phone Number')</label>
                                    <div class="input-group mb-3">
                                        <input type="tel" name="phone" class="form-control" placeholder="@lang('Enter Phone Number')" aria-label="tel" aria-describedby="tel" required>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>

                    <div class="card my-4">
                        <div class="card-body">
                            <h5 class="card-title text--base">@lang('Have a voucher code?')</h5>
                            <div class="row">
                                <div class="input-group mb-3">
                                    <input type="text" name="voucher" class="form-control" placeholder="@lang('Voucher Code?')" aria-label="voucher" aria-describedby="voucher">
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title text--base">@lang('Total')</h5>

                            <div class="row">
                                @foreach($ticket->seats['passangers'] as $passanger => $count)
                                @if($count > 0)
                                {{-- <p class="col-md-6">1 x Normal</p> --}}
                                <p class="col-md-6">{{$count}} x @lang($passanger)</p>
                                <p class="col-md-6 d-flex flex-row-reverse">{{number_format($ticket->seats[$passanger.'_total'])}} RSD</p>
                                @endif
                                @endforeach

                            </div>

                            <div class="row dotted-border pb-3">

                                <p class="col-md-6">@lang('Booking fee')</p>
                                <p class="col-md-6 d-flex flex-row-reverse"> + {{number_format($ticket->sub_total - $ticket->unit_price)}} RSD </p>

                            </div>
                            <div class="row py-3">

                                <p class="col-md-6"><b>@lang('Total')</b> (@lang('fees included, payment in RSD'))</p>
                                <p class="col-md-6 d-flex flex-row-reverse"><b>{{ number_format(round($ticket->sub_total,2))}} RSD</b></p>

                            </div>

                        </div>
                    </div>




                    <div class="d-flex flex-column m-2">
                        {{-- <div class="row">
                            <div class="col-1">
                                <input class="m-1 form-check-input" type="checkbox" name="review_link">
                            </div>
                            <div class="col px-1">
                                @lang('I agree to receive a trip review link')
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="col-1">
                            <input class="m-1 form-check-input" type="checkbox" name="email_updates">
                            </div>
                            <div class="col px-1">
                                @lang('Be the first to receive limited offers and other updates by email')
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-1">
                            <input class="m-1 form-check-input" type="checkbox" name="terms">
                            </div>
                            <div class="col px-1">
                                @lang('I have read and agreed to the') <span class="text-info">@lang('General terms')</span> @lang('and') <span class="text-info">@lang('Privacy policy')</span>
                            </div>
                        </div>
                        {{-- <button class="cmn--btn my-3">Confirm & Pay 20.2 EUR</button> --}}
                        <button type="submit" class="btn cmn--btn my-3">@lang('Confirm & Pay') {{round($ticket->sub_total,2)}} RSD</button>
                    </div>

                </form>
            </div>  {{-- col-lg-8 --}}


            <div class="col-12 col-md-5 col-lg-4">
                <!-- info -->
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text--base">@lang('Ticket code'): <span class="text-primary mx-2">{{$ticket->pnr_number}}</span></h5>
                        <h4 class="my-3 pb-3 text--base dotted-border">@lang('Outbound trip info'):</h4>
{{--                        <?php dd($ticket->trip->schedule);?>--}}
                        <div class="row">
                            <div class="col-5 txt-size dotted-border2">
                                @php

                                    $startStoppage = $ticket->trip->stoppages->where('counter_id',$ticket->pickup_point)->first();
                                    $endStoppage = $ticket->trip->stoppages->where('counter_id', $ticket->dropping_point)->first();

                                        $day_diff = strtotime($endStoppage->arrival) < strtotime($startStoppage->departure) ? 1 :  0;


                                        $date_start = date_create($ticket->date_of_journey);
                                         $date_start = date_format($date_start,'d M');
                                        $date_end = (new DateTime($ticket->date_of_journey))->modify('+1 day');
                                        $date_end = $day_diff ? date_format($date_end,'d M') : $date_start; //checks if needs to show next day date on trip

                                            $startTime = ($ticket->pickup_point == $ticket->trip->start_from) ? $ticket->trip->schedule->start_from : $ticket->trip->stoppages->where('counter_id', $ticket->pickup_point)->first() ? $ticket->trip->stoppages->where('counter_id', $ticket->pickup_point)->first()->departure : '00:00';
                                            $endTime = ($ticket->dropping_point == $ticket->trip->end_to) ? $ticket->trip->schedule->end_at : $ticket->trip->stoppages->where('counter_id', $ticket->dropping_point)->first() ? $ticket->trip->stoppages->where('counter_id', $ticket->dropping_point)->first()->arrival : '00:00';
                                @endphp
                                <p>{{showDateTime($ticket->date_of_journey,'d M') }} | {{showDateTime($startTime,'H:i')}}</p>
                                {{-- <p class="m-3 bg-info rounded-pill">5h 55min</p> --}}
                                <p>{{showDateTime($date_end, 'd M')}} | {{showDateTime($endTime,'H:i')}}</p>

                            </div>

                            <div class="col-5 txt-size">
                                <p>{{$ticket->pickup->name}}</p>
                                {{-- <span class="my-3 px-3 bg-warning rounded-pill">SIR</span> --}}
                                <p>{{$ticket->drop->name}}</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div> {{-- /.row --}}
    </div> {{-- /.container --}}
</section>



@endsection
@push('style')
    <style>
        .dotted-border {
            border-width: 2px;
            border-bottom-style: dotted;
            border-color: #eef2fb;
        }

        .dotted-border2 {
            border-width: 2px;
            border-right-style: dotted;
            border-color: #eef2fb;
        }

        .txt-size {
            font-size: 12px;
            text-align: center;
        }

        .txt-size2 {
            font-size: 12px;
            text-align: left;
        }
    </style>

@endpush


@push('script')

@endpush
