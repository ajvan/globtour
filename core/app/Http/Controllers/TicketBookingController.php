<?php

namespace App\Http\Controllers;


use App\Lib\BusLayout;
use App\Models\AdminNotification;
use App\Models\FleetType;
use App\Models\Frontend;
use App\Models\Language;
use App\Models\Page;
use App\Models\Schedule;
use App\Models\SupportMessage;
use App\Models\SupportTicket;
use App\Models\Trip;
use App\Models\TicketPrice;
use App\Models\BookedTicket;
use App\Models\VehicleRoute;
use App\Models\Counter;
use App\Models\NestpayPayment;

use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Knp\Snappy\Pdf;

class TicketBookingController extends Controller
{

    public function __construct()
    {
        $this->activeTemplate = activeTemplate();
    }



    /* Custom */
    public function orderTicket(Request $request) {
        //dd($request->all());
        $request->validate([
            "start"   => "required|integer|gt:0",
            "end"  => "required|integer|gt:0",
            "start_date" => "required|date",
        ]);

        // if(!auth()->user()){
        //     $notify[] = ['error', 'Without login you can\'t book any tickets'];
        //     return redirect()->route('user.login')->withNotify($notify);
        // }

        $date_of_journey  = Carbon::parse($request->start_date);
        $today            = Carbon::today()->format('Y-m-d');
        if($date_of_journey->format('Y-m-d') < $today ){
            $notify[] = ['error', 'Date of journey cant\'t be less than today.'];
            return redirect()->back()->withNotify($notify);
        }

        if(!(array_sum($request->passangers) > 0)) {
            $notify[] = ['error', 'At least one passanger is required.'];
            return redirect()->back()->withNotify($notify);
        }

        $trip   = Trip::findOrFail($request->trip);
        $ticket = $this->createTicket($request, $trip);

        $notify[] = ['success', 'Ticket booked successfully, please continue with payment instructions!'];
        //return redirect()->route('user.home')->withNotify($notify);
        return redirect()->route('ticket.details', $ticket->pnr_number)->withNotify($notify);
    }


    public function orderDetails($pnr) {
        //dd($pnr);
        $ticket = BookedTicket::where('pnr_number', $pnr)->first();
        if($ticket->status == 2)
        {
            $pageTitle = 'Payment Details';
            return view($this->activeTemplate.'payment_details', compact('pageTitle', 'ticket'));
        } else {
            $notify[] = ['error', 'Ticket not available or expired.'];
            return redirect()->back()->withNotify($notify);
        }
    }




    private function createTicket(Request $request, $trip) {

        $pnr_number = getTrx(10);
        $details = $this->createTicketDetails($trip, $request->start, $request->end, $request->passangers, $request->bags);

        $bookedTicket = new BookedTicket();
        $bookedTicket->user_id = auth()->user() ? auth()->user()->id : 1;
        $bookedTicket->gender = 0;
        $bookedTicket->trip_id = $trip->id;
        $bookedTicket->source_destination = [$request->start, $request->end];
        $bookedTicket->pickup_point = $request->start;
        $bookedTicket->dropping_point = $request->end;
        $bookedTicket->seats = $details;
        $bookedTicket->ticket_count = array_sum($request->passangers);
        $bookedTicket->unit_price = $details['total'];
        $bookedTicket->price_fee = $details['total'] * ($trip['price_fee'] / 100);
        $bookedTicket->sub_total = $bookedTicket->unit_price + $bookedTicket->price_fee;
        $bookedTicket->date_of_journey = Carbon::parse($request->start_date)->format('Y-m-d');
        $bookedTicket->date_of_return = $request->end_date ? Carbon::parse($request->end_date)->format('Y-m-d') : null;
        $bookedTicket->round_trip = $request->end_date ? 1 : 0;
        $bookedTicket->pnr_number = $pnr_number;
        $bookedTicket->status = 2;
        $bookedTicket->save();

        return $bookedTicket->refresh();
    }


    private function createTicketDetails($trip, $source, $destination, $passangers, $bags) {
        $adults = $trip->ticketPrice($source, $destination, 'odrasli');
        $students = $trip->ticketPrice($source, $destination, 'studenti');
        $adultPrice = $adults * $passangers['adults'];
        $studentPrice = $students * $passangers['students'];
        $childrenPrice = ($adults * 0.5) * $passangers['children'];
        $retiredPrice = ($adults * 0.5) * $passangers['retired'];

        return [
            'passangers' => $passangers,
            'bags' => (int) $bags,
            //'fee' => $trip->fee ?? 0,
            'total' => $adultPrice + $studentPrice +  $retiredPrice + $childrenPrice + ($bags *120),
            // prices
            'adults' => $adults,
            'children'  => $adults * 0.5,
            'students'  => $students,
            'retired' => $adults * 0.5,
            // total
            'adults_total' => $adultPrice,
            'students_total' => $studentPrice,
            'children_total' => $childrenPrice,
            'retired_total' => $retiredPrice,
        ];
    }



    public function ticketDesign(){

        $template = $this->activeTemplate.'layouts.ticket-design';
        $dompdf = new Dompdf();
        $dompdf->loadHtml(view($template));
        $dompdf->setPaper('A3');
        $dompdf->render();
        $dompdf->stream();

         return view($this->activeTemplate.'layouts.ticket-design');
    }

    /**
     * Ticket print -> acctually prints a ticket
     */

    public function ticketPrint($code = null){
        if($code) {
            $ticket = BookedTicket::where('pnr_number', $code)->first();

            if($ticket) {
                $payment = NestpayPayment::where('ticket_id', $ticket->id)->first();

                if($payment) {
                    $template = $this->activeTemplate.'layouts.ticket-print';
                    $dompdf = new Dompdf();
                    $dompdf->loadHtml(view($template)->with('ticket', $ticket)->with('payment', $payment));
                    $dompdf->setPaper('A3');
                    $dompdf->render();
                    $dompdf->stream();
                    return view($this->activeTemplate.'layouts.ticket-print')->with('ticket', $ticket)->with('payment', $payment);

                } else {
                    return 'no payment found!';
                }

            } else {
                return 'no ticket found!';
            }
        }
        return 'no code!';
    }


    public function ticketCheck($code = null) {
        if($code) {
            $ticket = BookedTicket::where('pnr_number', $code)->first();
            if($ticket) {
                $payment = NestpayPayment::where('ticket_id', $ticket->id)->first();
                if($payment) {
                    if($ticket->status == 1 && $payment->processed == 1 && $payment->Response == 'Approved') {
                        return 'ticket approved!';
                    } else if ($ticket->status == 2 || $payment->processed == 0) {
                        return 'ticket still pending.';
                    } else {
                        return 'ticket not approved, contact support.';
                    }
                }
            }
        }
        return 'no ticket or payment not found.';
    }


}
