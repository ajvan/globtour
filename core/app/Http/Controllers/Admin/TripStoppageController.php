<?php

namespace App\Http\Controllers\Admin;

use App\Models\Trip;
use App\Models\TripStoppage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TripStoppageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tripId)
    {
        $trip = Trip::findOrFail($tripId);
        if($trip->stoppages->isEmpty() && !empty($trip->route->stoppages)) {
            $this->create($trip);
        }

        $trip->refresh();
        $trip->load('stoppages');
        $stoppages = $trip->stoppages->sortBy('order');
        $pageTitle = 'Route Stoppages';
        return view('admin.trip.stoppages', compact('pageTitle','trip', 'stoppages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($trip)
    {
        $i = 1;
        foreach($trip->route->stoppages as $stoppage) {
            TripStoppage::create([
                'counter_id'        => $stoppage,
                'trip_id'           => $trip->id,
                'vehicle_route_id'  => $trip->route->id,
                'order'             => $i
            ]);
            $i++;
        }
        return;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TripStoppage  $tripStoppage
     * @return \Illuminate\Http\Response
     */
    public function show(TripStoppage $tripStoppage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TripStoppage  $tripStoppage
     * @return \Illuminate\Http\Response
     */
    public function edit(TripStoppage $tripStoppage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TripStoppage  $tripStoppage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //         $request->validate($request, [
//             'times.*' => 'date_format:H:i',
//         ]);

        foreach($request->times as $stoppage => $times) {
            $toUpdate = TripStoppage::find($stoppage);
            $times = (array) $times;
            $toUpdate->arrival = $times[0];
            $toUpdate->departure = $times[1];
            // crossover
            if(isset($request->is_active_crossover[$stoppage])) {
                $toUpdate->crossover = array_key_last($request->is_active_crossover[$stoppage]) == 1 ? 1 : 0;
            }
            $toUpdate->save();
        }
        return redirect()->route('admin.trip.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TripStoppage  $tripStoppage
     * @return \Illuminate\Http\Response
     */
    public function destroy(TripStoppage $tripStoppage)
    {
        //
    }
}
