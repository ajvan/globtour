<?php

namespace App\Http\Controllers\Admin;

use App\Models\AccommodationMedia as Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AccommodationMedia;

class AccommodationMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $media = new Media;
        $media->accommodation_id = $id;

        if ($request->hasFile('file')) {
            $image = $request->file('file');

            // Get filename with the extension
            $fileNameWithExt = $image->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // Get just extension
            $ext = $image->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = uniqid().'-'.time().'-'.$filename.'.'.$ext;

            $request->file('file')->move('assets/images/accommodations', $fileNameToStore);
            $media->file = $fileNameToStore;
            $media->type = 'image';
            $media->title = $filename;
            $media->url = asset('assets/images/accommodations/'.$fileNameToStore);
        }
        $media->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AccommodationMedia  $accommodationMedia
     * @return \Illuminate\Http\Response
     */
    public function show(Media $accommodationMedia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AccommodationMedia  $accommodationMedia
     * @return \Illuminate\Http\Response
     */
    public function edit(Media $accommodationMedia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AccommodationMedia  $accommodationMedia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Media $accommodationMedia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AccommodationMedia  $accommodationMedia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $accommodationMedia = AccommodationMedia::findOrFail($id);
        $accommodationMedia->delete();
        $notify[] = ['success', 'Media removed successfully.'];
        return back()->withNotify($notify);
    }
}
