<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{

    public function index()
    {
        $pageTitle = 'All Articles';
        $emptyMessage = 'No Articles found';
        $articles = Article::all();
        return view('admin.article.list', compact('pageTitle','emptyMessage','articles'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:articles',
        ]);

        $article = new Article();
        $article->title       =  $request->title;
        $article->slug        =  slug($request->title);
        $article->lang        =  $request->lang;
        $article->description =  $request->description;
        $article->save();

        $notify[] = ['success', 'Article saved successfully.'];
        return back()->withNotify($notify);
    }


    public function show($id)
    {
            $article = Article::findOrFail($id);
            $pageTitle = 'Article';
            $emptyMessage = 'No Article found';
            return view('admin.article.show', compact('pageTitle','emptyMessage','article'));
    }


    public function edit(Request $request, $id)
    {
       //dd($request->all());
       $article = Article::findOrFail($id);

       // image store here
       if ($request->hasFile('image')) {
           $imagePath = $this->storeImage($request);
           $article->image = $imagePath;
       }


       $article->content = $request->content;
       $article->title = $request->title;
       $article->lang = $request->lang;
       $article->slug = slug($request->title);
       $article->save();

       $notify[] = ['success', 'Article updated successfully.'];
       return back()->withNotify($notify);
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required'
        ]);

        $article = Article::find($id);
        $article->title         =  $request->title;
        $article->description   =  $request->description;
        $article->save();

        $notify[] = ['success', 'Article update successfully.'];
        return back()->withNotify($notify);
    }


    public function destroy(Request $request)
    {
           $article = Article::findOrFail($request->id);
           $article->delete();
           $notify[] = ['success', 'Article deleted successfully'];
           return back()->withNotify($notify);
    }

    private function storeImage($request)
    {
        $image = $request->file('image');
        // Get filename with the extension
        $fileNameWithExt = $image->getClientOriginalName();
        // Get just filename
        $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
        // Get just extension
        $ext = $image->getClientOriginalExtension();
        // Filename to store
        $fileNameToStore = uniqid().'-'.time().'-'.$filename.'.'.$ext;

        $request->file('image')->move('assets/images/blog', $fileNameToStore);
        $path = asset('assets/images/blog/' . $fileNameToStore);
        return $path;
    }
}
