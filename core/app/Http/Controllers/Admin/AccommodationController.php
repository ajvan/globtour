<?php

namespace App\Http\Controllers\Admin;

use App\Models\Accommodation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AccommodationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'All Accommodation';
        $emptyMessage = 'No Accommodation found';
        //$Accommodations = Accommodation::paginate(getPaginate());
        $accommodations = Accommodation::all();
        return view('admin.accommodation.list', compact('pageTitle','emptyMessage','accommodations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:accommodations',
            'city' => 'required',
            //'mobile' => 'required|numeric|unique:accommodations'
        ]);

        $accommodation = new Accommodation();
        $accommodation->name        =  $request->name;
        $accommodation->slug        =  slug($request->name);
        $accommodation->lang        =  slug($request->lang);
        $accommodation->city        =  $request->city;
        $accommodation->location    =  $request->location;
        $accommodation->mobile      =  $request->mobile;
        $accommodation->save();

        $notify[] = ['success', 'Accommodation save successfully.'];
        return back()->withNotify($notify);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Accommodation  $accommodation
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $accommodation = Accommodation::findOrFail($id);
        $pageTitle = 'Accommodation';
        $emptyMessage = 'No Accommodation found';
        return view('admin.accommodation.show', compact('pageTitle','emptyMessage','accommodation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Accommodation  $accommodation
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //dd($request->all());
        $accommodation = Accommodation::findOrFail($id);

        // image store here
        if ($request->hasFile('image')) {
            $imagePath = $this->storeImage($request);
            $accommodation->image = $imagePath;
        }


        $accommodation->description = $request->description;
        $accommodation->name = $request->name;
        $accommodation->lang = $request->lang;
        $accommodation->slug = slug($request->name);
        $accommodation->save();

        $notify[] = ['success', 'Accommodation updated successfully.'];
        return back()->withNotify($notify);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:accommodations,name,'.$id,
            'city' => 'required',
            //'mobile' => 'required|numeric|unique:accommodations,mobile,'.$id
        ]);

        $accommodation = Accommodation::find($id);
        $accommodation->name      =  $request->name;
        $accommodation->slug      =  slug($request->name);
        $accommodation->lang      =  $request->lang;
        $accommodation->city      =  $request->city;
        $accommodation->location  =  $request->location;
        $accommodation->mobile    =  $request->mobile;
        $accommodation->save();

        $notify[] = ['success', 'Accommodation update successfully.'];
        return back()->withNotify($notify);
    }

    public function activeDisabled(Request $request){
        $request->validate(['id' => 'required|integer']);

        $accommodation = Accommodation::find($request->id);
        $accommodation->status = $accommodation->status == 1 ? 0 : 1;
        $accommodation->save();

        if($accommodation->status == 1){
            $notify[] = ['success', 'Accommodation active successfully.'];
        }else{
            $notify[] = ['success', 'Accommodation disabled successfully.'];
        }

        return back()->withNotify($notify);
    }

    private function storeImage($request)
    {
        $image = $request->file('image');
        // Get filename with the extension
        $fileNameWithExt = $image->getClientOriginalName();
        // Get just filename
        $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
        // Get just extension
        $ext = $image->getClientOriginalExtension();
        // Filename to store
        $fileNameToStore = uniqid().'-'.time().'-'.$filename.'.'.$ext;

        $request->file('image')->move('assets/images/accommodations', $fileNameToStore);
        $path = asset('assets/images/accommodations/' . $fileNameToStore);
        return $path;
    }

}
