<?php

namespace App\Http\Controllers;

use App\Models\AdminNotification;
use App\Models\FleetType;
use App\Models\Frontend;
use App\Models\Language;
use App\Models\Page;
use App\Models\Schedule;
use App\Models\SupportMessage;
use App\Models\SupportTicket;
use App\Models\Trip;
use App\Models\TicketPrice;
use App\Models\BookedTicket;
use App\Models\VehicleRoute;
use App\Models\Counter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FrontController extends Controller
{
    public function __construct(){
        $this->activeTemplate = activeTemplate();
    }

    /**
     * New ticket search function
     */
    public function ticketSearch(Request $request)
    {

        if($request->pickup && $request->destination && $request->pickup == $request->destination){
            $notify[] = ['error', 'Please select pickup point and destination point properly'];
            return redirect()->back()->withNotify($notify);
        }

        if($request->date_of_journey && Carbon::parse($request->date_of_journey)->format('Y-m-d') < Carbon::now()->format('Y-m-d')){
            $notify[] = ['error', 'Date of journey can\'t be less than today.'];
            return redirect()->back()->withNotify($notify);
        }else if(!$request->date_of_journey){
            $notify[] = ['error', 'Please select departure date.'];
            return redirect()->back()->withNotify($notify);
        }

        // proveriti za datum povratka
        if($request->date_of_return && Carbon::parse($request->date_of_return)->format('Y-m-d') < Carbon::parse($request->date_of_journey)->format('Y-m-d')){
            $notify[] = ['error', 'Date of return can\'t be less than the date of journey.'];
            return redirect()->back()->withNotify($notify);
        }


        $trips = Trip::active();

        if($request->pickup && $request->destination) {
            Session::flash('pickup', $request->pickup);
            Session::flash('destination', $request->destination);

            $pickup = $request->pickup;
            $destination = $request->destination;
            $trips = $trips->with('route', 'stoppages')->get();
            $tripArray = array();
            foreach ($trips as $trip) {
                $startPoint = $trip->stoppages->where('counter_id', $pickup)->first();
                $endPoint =  $trip->stoppages->where('counter_id', $destination)->first();
                if($startPoint && $endPoint && ((int)$startPoint->order < (int)$endPoint->order)) {
                    $price = $trip->ticketPrice($pickup, $destination);
                    if($price > 0) {
                        $tripArray[] = $trip->id;
                    }
                }
            }
            //exact search
            $trips = Trip::active()->whereIn('id', $tripArray);
        } else {
            // old search
            if($request->pickup){
                Session::flash('pickup', $request->pickup);
                $pickup = $request->pickup;
                $trips = $trips->whereHas('route' , function($route) use ($pickup){
                    $route->whereJsonContains('stoppages' , $pickup);
                });
            }

            if($request->destination){
                Session::flash('destination', $request->destination);
                $destination = $request->destination;
                $trips = $trips->whereHas('route' , function($route) use ($destination){
                    $route->whereJsonContains('stoppages' , $destination);
                });
            }
        }

        // search preferences
        if($request->fleetType){
            $trips = $trips->whereIn('fleet_type_id',$request->fleetType);
        }

        if($request->routes){
            $trips = $trips->whereIn('vehicle_route_id',$request->routes);
        }

        if($request->schedules){
            $trips = $trips->whereIn('schedule_id',$request->schedules);
        }

        if($request->date_of_journey){
            Session::flash('date_of_journey', $request->date_of_journey);
            $dayOff = Carbon::parse($request->date_of_journey)->format('w');
            $trips = $trips->whereJsonDoesntContain('day_off', $dayOff);
        }



        // out
        $trips = $trips->with( ['fleetType' ,'route', 'schedule', 'startFrom' , 'endTo', 'stoppages'] )->where('status', 1)->paginate(getPaginate());

        $pageTitle = 'Search Result';
        $emptyMessage = 'There is no trip available';
        $fleetType = FleetType::active()->get();
        $schedules = Schedule::all();
        $routes = VehicleRoute::active()->get();

        $startEnd = Counter::find([$request->pickup, $request->destination]);
        $counters = Counter::active()->get();

        $date_of_journey = $request->date_of_journey;
        $date_of_return = $request->date_of_return;
        if(auth()->user()){
            $layout = 'layouts.master';
        }else{
            $layout = 'layouts.frontend';
        }

        return view($this->activeTemplate.'ticket', compact('pageTitle' ,'fleetType', 'trips','routes', 'schedules', 'startEnd', 'emptyMessage', 'layout', 'counters', 'date_of_journey','date_of_return'));
    }
}
