<?php

namespace App\Http\Controllers;

use App\Models\AdminNotification;
use App\Models\GeneralSetting;
use App\Models\User;
use App\Models\UserLogin;
use App\Models\BookedTicket;
use Cubes\Nestpay\MerchantService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class NestpayController extends Controller
{
	public function confirment(Request $request, $pnr)
	{
        // get ticket
        $ticket = BookedTicket::where('pnr_number', $pnr)->first();

        if(!$ticket) {
            abort(404);
        }

        $request->validate([
            'email' => 'required|email|regex:/(.+)@(.+)\.(.+)/i',
            'terms' => 'required',
        ]);

        if(!Auth::check()) {
            // check for user in the database
            $user = User::where('email', $request->email)->first();

            // if no user create new user
            if(!$user) {
                $user = $this->createNewUser($request->all());
            }

            // log in user and continue as authenticated
            // Moved to later stage
            Auth::login($user);
        } else {
            $user = Auth::user();
        }


        // update ticket details from user
        $ticket->user_id = $user->id;
        $ticket->save();


        // update payment data with voucher
        // to do...

        //this is start route where payment is confirmed
        //the form is pouplated by ajax from confirm action
        $paymentData = $this->getPaymentData($ticket, $user);
        return view('nestpay.confirm', ['paymentData' => $paymentData, 'ticket' => $ticket]);
    }

    /**
     * This is ajax route
     * When payment is confirmed on previous page
     * the form parameters are generated and returned as json
     * then the form is populated and submited (POST-ed) to the nestpay 3d secure page.
     * This is done to avoid unnecessary creation of OID (nestpay_payments table records)
     *
     * @param MerchantService $nestpayMerchantService
     * @param Request $request
     * @return json
     */
    public function confirm(MerchantService $nestpayMerchantService, Request $request)
    {
        // ovo razmotriti
        if(!Auth::check()) {
            dd('user not logged in');
        }

        $user = Auth::user();
        $ticket = BookedTicket::where('pnr_number', $request->pnr)->first();

        // if no ticket return
        if(!$ticket) {
            dd('no ticket found');
        }

        $paymentData = $this->getPaymentData($ticket, $user);

        //change the routes for success and fail    - DONE!
        $nestpayMerchantService->getMerchantConfig()->setConfig([
			'okUrl' => route('nestpay.success', $request->pnr),
			'failUrl' => route('nestpay.fail', $request->pnr),
		]);

        $formFields = $nestpayMerchantService->paymentMakeRequestParameters($paymentData);

        /**
         * The working payment is created at this point
         * Set specific columns for nestpay model, like user_id , order_id etc
         */
        $nestpayPayment = $nestpayMerchantService->getWorkingPayment();
        $nestpayPayment->fill([
          'user_id'     => $user->id,
          'ticket_id'   => $ticket->id
        ]);
        $nestpayPayment->save();

        return response()->json($formFields);
    }

    /**
     * This is successfull processing
     *
     *
     * @param MerchantService $nestpayMerchantService
     * @param Request $request
     * @return view
     */
    public function success(MerchantService $nestpayMerchantService, Request $request, $pnr = null)
    {
        $payment = null;
        $ex = null;

        // get ticket
        if($pnr) {
            $ticket = BookedTicket::where('pnr_number', $pnr)->first();
            if($ticket) {
                $ticket->status = 1;
                $ticket->save();
            }
        }

		try {
            $payment = $nestpayMerchantService->paymentProcess3DGateResponse($request->all());

            //the payment has been process successfully
            //THAT DOES NOT MEAN THAT CUSTOMER HAS PAID!!!!
            //FOR SUCCESSFULL PAYMENT SEE \App\Listeners\NestpayEventSubscriber!!!
            //DO NOT ADD CODE HERE FOR SUCCESSFULL PAYMENT!!!!

		} catch (\Cubes\Nestpay\PaymentAlreadyProcessedException $ex) {
            //the payment has been already processed
            //this error occures if customer refresh result page
            //add code here for the case if necessary
            //$ex = null;//comment this if you want to show this exception if debug is on
		} catch (\Exception $ex) {
			//any other error
            //add code here for the case if necessary
		} finally {
            //try to get working payment

            try {
				$payment = $nestpayMerchantService->getWorkingPayment();
			} catch (\Exception $exTemp) {}
        }

        if ($ex && config('app.debug')) {
            //if debug is enabled throw exception
            throw $ex;
        }

        return view('nestpay.result', [
            'payment' => $payment,
            'exception' => $ex,
            'pnr'       => $pnr
        ]);
    }

    /**
     * The fiail url
     * Process payment even in this case!!!
     *
     * @param MerchantService $nestpayMerchantService
     * @param Request $request
     * @return void
     */
    public function fail(MerchantService $nestpayMerchantService, Request $request, $pnr = null)
    {
        $payment = null;
        $ex = null;

        // get ticket
        if($pnr) {
            $ticket = BookedTicket::where('pnr_number', $pnr)->first();
            if($ticket) {
                $ticket->status = 0;
                $ticket->save();
            }
        }


		try {
			$payment = $nestpayMerchantService->paymentProcess3DGateResponse($request->all());

		} catch (\Cubes\Nestpay\PaymentAlreadyProcessedException $ex) {
            //the payment has been already processed
            //this error occures if customer refresh result page
            //add code here for the case if necessary
            $ex = null;//comment this if you want to show this exception if debug is on

		} catch (\Exception $ex) {
			//any other error
            //add code here for the case if necessary

		} finally {
            //try to get working payment

            try {
				$payment = $nestpayMerchantService->getWorkingPayment();
			} catch (\Exception $exTemp) {}
        }

        if ($ex && config('app.debug')) {
            //if debug is enabled throw exception
            throw $ex;
        }

        return view('nestpay.result', [
            'payment' => $payment,
            'exception' => $ex,
            'isFail' => true
        ]);
    }

    /**
     * This is just a helper function forCinitial example to work
     * Change the details in production
     *
     * @return array
     */
    protected function getPaymentData($ticket, $user)
    {
        return [
            \Cubes\Nestpay\Payment::PROP_TRANTYPE => \Cubes\Nestpay\Payment::TRAN_TYPE_PREAUTH, //two step processing
            //\Cubes\Nestpay\Payment::PROP_TRANTYPE => \Cubes\Nestpay\Payment::TRAN_TYPE_AUTH, //single step processing

            //Below is required data for payment

            \Cubes\Nestpay\Payment::PROP_AMOUNT => round($ticket->sub_total,2),
            \Cubes\Nestpay\Payment::PROP_CURRENCY => \Cubes\Nestpay\Payment::CURRENCY_RSD,
            //change with the name of your customer (reading from config is just for example)
            //\Cubes\Nestpay\Payment::PROP_BILLTONAME => config('mail.from.name', 'FirstName LastName'),
            \Cubes\Nestpay\Payment::PROP_BILLTONAME => $user->full_name ?? $user->username,
            //change with email of your customer (reading from config is just for example)
			//\Cubes\Nestpay\Payment::PROP_EMAIL => config('mail.from.address', 'mailbox@example.com'),
            \Cubes\Nestpay\Payment::PROP_EMAIL => $user->email,



            //Below is optional data for payment (here are added for example)

            //This is good practice to read language from app locale
			\Cubes\Nestpay\Payment::PROP_LANG => app()->getLocale(),

			//\Cubes\Nestpay\Payment::PROP_INVOICENUMBER => '144566789', //MUST BE NUMERIC!!!
            //\Cubes\Nestpay\Payment::PROP_INVOICENUMBER => $ticket->pnr_number, //MUST BE NUMERIC!!!
            \Cubes\Nestpay\Payment::PROP_INVOICENUMBER => mt_rand(1000000, 9999999),    //TODO: change this
			\Cubes\Nestpay\Payment::PROP_DESCRIPTION => 'Globtour ticket order No. '.$ticket->pnr_number,

			// \Cubes\Nestpay\Payment::PROP_BILLTOSTREET1 => 'My Street',
			// \Cubes\Nestpay\Payment::PROP_BILLTOCITY => 'My City',
			// \Cubes\Nestpay\Payment::PROP_BILLTOCOUNTRY => 'My Country',
			// \Cubes\Nestpay\Payment::PROP_TEL => '1 55 555 555',
        ];
    }



    /**
     * Redirect back helper
     */

    public function redirectBack($pnr=null) {
        if($pnr) {
            $ticket = BookedTicket::where('pnr_number', $pnr)->first();
            if($ticket) {
                $user = User::find($ticket->user_id);
                if($user) {
                    Auth::login($user);
                    return redirect()->route('user.home');
                }
            }
        }
        return redirect()->route('home');
    }

    /**
     * Create new user helper
     */

    private function createNewUser($data) {


        $general = GeneralSetting::first();
          //User Create
          $user = new User();
          $user->firstname = isset($data['first_name']) ? $data['first_name'] : null;
          $user->lastname = isset($data['last_name']) ? $data['last_name'] : null;
          $user->email = strtolower(trim($data['email']));
          $user->password = Hash::make('password123');
          $user->username = 'Guest'.User::all()->count();
          //$user->country_code = $data['country_code'];
          //$user->mobile = $data['mobile_code'].$data['mobile'];
          $user->mobile = $data['phone'] ?? '';
          $user->address = [
              'address' => '',
              'state' => '',
              'zip' => '',
              'country' => '',
              'city' => ''
          ];
          $user->status = 1;
          $user->ev = $general->ev ? 0 : 1;
          $user->sv = $general->sv ? 0 : 1;
          $user->save();
          $user->refresh();


          $adminNotification = new AdminNotification();
          $adminNotification->user_id = $user->id;
          $adminNotification->title = 'New member registered';
          $adminNotification->click_url = urlPath('admin.users.detail',$user->id);
          $adminNotification->save();


          //Login Log Create
          $ip = $_SERVER["REMOTE_ADDR"];
          $exist = UserLogin::where('user_ip',$ip)->first();
          $userLogin = new UserLogin();

          //Check exist or not
          if ($exist) {
              $userLogin->longitude =  $exist->longitude;
              $userLogin->latitude =  $exist->latitude;
              $userLogin->city =  $exist->city;
              $userLogin->country_code = $exist->country_code;
              $userLogin->country =  $exist->country;
          }else{
              $info = json_decode(json_encode(getIpInfo()), true);
              $userLogin->longitude =  @implode(',',$info['long']);
              $userLogin->latitude =  @implode(',',$info['lat']);
              $userLogin->city =  @implode(',',$info['city']);
              $userLogin->country_code = @implode(',',$info['code']);
              $userLogin->country =  @implode(',', $info['country']);
          }

          $userAgent = osBrowser();
          $userLogin->user_id = $user->id;
          $userLogin->user_ip =  $ip;

          $userLogin->browser = @$userAgent['browser'];
          $userLogin->os = @$userAgent['os_platform'];
          $userLogin->save();


          return $user;
    }
}
