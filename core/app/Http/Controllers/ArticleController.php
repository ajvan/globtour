<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{


    public function __construct(){
        $this->activeTemplate = activeTemplate();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = session()->get('lang');
        $articles = Article::where('lang', $lang)->get();
        if($articles->isNotEmpty()) {
            $pageTitle = 'Blogs';
            return view($this->activeTemplate.'article', compact('pageTitle', 'articles'));
        }

        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $lang = session()->get('lang');
        //$article = Article::where('slug', $slug)->first();
        $all = Article::where('lang', $lang)->get();
        $article = $all->where('slug', $slug)->first();

        if($article) {
            $articles = $all->where('id', '!=', $article->id);

            $pageTitle = $article->title;
            if(auth()->user()){
                $layout = 'layouts.master';
            }else{
                $layout = 'layouts.frontend';
            }
            return view($this->activeTemplate.'article_details', compact('pageTitle', 'layout', 'article', 'articles'));
        }

        abort(404);
    }


}
