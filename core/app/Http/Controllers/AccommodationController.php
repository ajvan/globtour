<?php

namespace App\Http\Controllers;

use App\Models\Accommodation;
use Illuminate\Http\Request;

class AccommodationController extends Controller
{

    public function __construct(){
        $this->activeTemplate = activeTemplate();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lang = session()->get('lang');
        $accommodations = Accommodation::active()->get();
        $selected = $accommodations->isNotEmpty() ? $accommodations->where('lang', $lang) : null;
        $accommodation = $selected->isNotEmpty() ? $selected->random() : null;

            $pageTitle = $accommodation->name ?? 'Accomodations';
            return view($this->activeTemplate.'accommodation', compact('pageTitle', 'accommodations', 'accommodation'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Accommodation  $accommodation
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $lang = session()->get('lang');
        $all = Accommodation::active()->get();
        $accommodations = $all->where('lang', $lang);
        $accommodation = $accommodations->isNotEmpty() ? $accommodations->where('slug', $slug)->where('lang', $lang)->first() : null;

        //dd($accommodation);
        if($accommodation) {
            $pageTitle = $accommodation->name;
            return view($this->activeTemplate.'accommodation', compact('pageTitle', 'accommodations', 'accommodation'));
        }

        abort(404);
    }


}
