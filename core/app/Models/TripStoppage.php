<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\VehicleRoute;
use App\Models\Counter;
use App\Models\Trip;


class TripStoppage extends Model
{
    use HasFactory;

    protected $fillable = ['counter_id', 'trip_id', 'vehicle_route_id', 'order', 'arrival', 'departure', 'status'];

    protected $with = [
        'counter'
    ];

    public function route() {
        return $this->belongsTo(VehicleRoute::class);
    }

    public function trip() {
        return $this->belongsTo(Trip::class);
    }

    public function counter() {
        return $this->belongsTo(Counter::class);
    }
}
