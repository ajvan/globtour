<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AccommodationMedia;

class Accommodation extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    //scope
    public function scopeActive(){
        return $this->where('status', 1);
    }

    public function media() {
        return $this->hasMany(AccommodationMedia::class);
    }

}
