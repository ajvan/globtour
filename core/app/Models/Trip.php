<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\TicketPrice;
use App\Models\TripStoppage;

class Trip extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    protected $casts = [
        'day_off' => 'array'
    ];

    protected $with = [
        'stoppages', 'startFrom', 'endTo', /*'counter'*/
    ];

    public function fleetType(){
        return $this->belongsTo(FleetType::class);
    }

    public function route(){
        return $this->belongsTo(VehicleRoute::class ,'vehicle_route_id' );
    }

    public function stoppages() {
        return $this->hasMany(TripStoppage::class);
    }

    public function schedule(){
        return $this->belongsTo(Schedule::class);
    }

    public function startFrom(){
        return $this->belongsTo(Counter::class, 'start_from', 'id');
    }

    public function endTo(){
        return $this->belongsTo(Counter::class, 'end_to', 'id');
    }

    public function assignedVehicle(){
        return $this->hasOne(AssignedVehicle::class);
    }

    public function bookedTickets(){
        return $this->hasMany(BookedTicket::class)->whereIn('status', [1,2]);
    }

    /* All tickets */
    public function getTicketPricesAttribute() {
        return TicketPrice::where('vehicle_route_id', $this->vehicle_route_id)->where('fleet_type_id', $this->fleet_type_id)->with('prices')->get();
    }

    /* Single ticket */
    public function ticketPrice($source, $destination, $type = 'odrasli') { // TODO!
        $price = TicketPrice::where('vehicle_route_id', $this->vehicle_route_id)->where('fleet_type_id', $this->fleet_type_id)->where('type', $type)->with('prices')->first();
        if($price) {
            // cela ruta
            if($this->start_from == $source && $this->end_to == $destination) {
                return $price->price;
            } else {
                $sdArray  = ["$source", "$destination"];
                $getPrice = $price->prices()->where('source_destination', json_encode($sdArray))->orWhere('source_destination', json_encode(array_reverse($sdArray)))->first();
                return $getPrice ? $getPrice->price : null;
            }
        }
        return null;
    }

    //scope
    public function scopeActive(){
        return $this->where('status', 1);
    }
}
