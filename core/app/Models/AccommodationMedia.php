<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Accommodation;

class AccommodationMedia extends Model
{
    use HasFactory;

    protected $table = "accommodation_media";
    protected $fillable = ['accommodation_id', 'file', 'url', 'type', 'title', 'alt', 'caption'];

    //public $timestamps = false;

    public function accommodation() {
        return $this->belongsTo(Accommodation::class);
    }
}
