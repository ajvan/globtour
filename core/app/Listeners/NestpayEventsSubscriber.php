<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Cubes\Nestpay\Laravel\NestpayPaymentProcessedSuccessfullyEvent;
use Cubes\Nestpay\Laravel\NestpayPaymentProcessedFailedEvent;
use Cubes\Nestpay\Laravel\NestpayPaymentProcessedErrorEvent;

use Cubes\Nestpay\Payment;
use App\Models\NestpayPayment;
use App\Models\BookedTicket;
use App\Mail\NestpayPaymentMail;

class NestpayEventsSubscriber
{
    /**
     * Successfull payment
     */
    public function nestpayPaymentProcessedSuccessfullyEvent(NestpayPaymentProcessedSuccessfullyEvent $event) {
        \Log::info('nestpay payment processed succesfully event triggered');
        $payment = $event->getPayment();

        //CUSTOMER HAS PAID, DO RELATED STUFF HERE

        // payment and ticket data
        $nestpayPayment = NestpayPayment::where('oid', $payment['oid'])->first();
        $ticket = BookedTicket::find($nestpayPayment->ticket_id);

        // another if check?

        $email = $payment->getProperty(Payment::PROP_EMAIL) ?? $nestpayPayment->email;
        $name = $payment->getProperty(Payment::PROP_BILLTONAME) ?? $nestpayPayment->BillToName;

        //sending email
        \Mail::to(
            //$payment->getProperty(Payment::PROP_EMAIL),
            //$payment->getProperty(Payment::PROP_BILLTONAME)
            $email,
            $name
        )->send(new NestpayPaymentMail($payment, $ticket));
    }

    /**
     * Failed payment
     */
    public function nestpayPaymentProcessedFailedEvent(NestpayPaymentProcessedFailedEvent $event) {
        \Log::info('nestpay payment processed failed event triggered');
        $payment = $event->getPayment();


        // payment and ticket data
        $nestpayPayment = NestpayPayment::where('oid', $payment['oid'])->first();
        $ticket = BookedTicket::find($nestpayPayment->ticket_id);


        // generate fail message

        $email = $payment->getProperty(Payment::PROP_EMAIL) ?? $nestpayPayment->email;
        $name = $payment->getProperty(Payment::PROP_BILLTONAME) ?? $nestpayPayment->BillToName;

        if($email && $name) {
            //sending email
            \Mail::to(
                $email,
                $name
            )->send(new NestpayPaymentMail($payment));
        }

    }

    /**
     * Error processing payment
     */
    public function nestpayPaymentProcessedErrorEvent(NestpayPaymentProcessedErrorEvent $event) {
        \Log::info('nestpay payment processed error event triggered');
        $payment = $event->getPayment(); //COULD BE NULL!!!
        $ex = $event->getException();


        \Log::error($ex);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Cubes\Nestpay\Laravel\NestpayPaymentProcessedSuccessfullyEvent',
            'App\Listeners\NestpayEventsSubscriber@nestpayPaymentProcessedSuccessfullyEvent'
        );

        $events->listen(
            'Cubes\Nestpay\Laravel\NestpayPaymentProcessedFailedEvent',
            'App\Listeners\NestpayEventsSubscriber@nestpayPaymentProcessedFailedEvent'
        );

        $events->listen(
            'Cubes\Nestpay\Laravel\NestpayPaymentProcessedErrorEvent',
            'App\Listeners\NestpayEventsSubscriber@nestpayPaymentProcessedErrorEvent'
        );
    }
}
