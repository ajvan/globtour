<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripStoppagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_stoppages', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('counter_id')->default(0);
            $table->unsignedInteger('trip_id')->default(0);
            $table->unsignedInteger('vehicle_route_id')->default(0);
            $table->unsignedInteger('order')->default(0);
            //$table->time('arrival')->nullable();
            //$table->time('departure')->nullable();
            $table->time('arrival')->default('00:00:00');
            $table->time('departure')->default('00:00:00');
            $table->tinyInteger('crossover')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_stoppages');
    }
}
